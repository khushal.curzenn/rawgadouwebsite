import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserServiceService } from '../user-service.service';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-filter-products',
  templateUrl: './filter-products.component.html',
  styleUrls: ['./filter-products.component.css']
})
export class FilterProductsComponent implements OnInit {
  baseurl:any;allSearchProducts: any;navURL:any;myoptions:any
  querystring:any;filterQuery:any;p:any;main_id:any
  myrating:any; minval:any; maxval:any; condition:any
  itemLength:any
  allproductsApi: any

  navURLRes:any
  constructor(private http: HttpClient,private user: UserServiceService,
    private activate:ActivatedRoute) { 
    this.baseurl = user.baseapiurl2
    this.navURL =this.baseurl+"api/product/listProducts"
    this.allproductsApi = this.baseurl + "api/product/getallproducts"
    
    this.querystring = this.activate.snapshot.params['id']
    this.filterQuery = this.activate.snapshot.params['id']
    this.allSearchProducts = []
  
    this.myoptions = {
      catagory: "",
      singleid: "",
      sort: 1,
      pricerangeLow: "",
      pricerangeHigh: "",
      rating: "",
      condition: ""
    }

    this.itemLength = ''
    this.myrating = ''
    this.minval =''
    this.maxval=''
    this.condition =''
  }

  ngOnInit(): void {
   // console.log(this.querystring)
    this.http.post(this.navURL,this.myoptions).subscribe(res=>{
      this.navURLRes = res
      this.allSearchProducts = this.navURLRes.data
      const filterData = this.allSearchProducts.filter((x:any)=>{  
        return x.title == this.querystring
      })
      if(filterData){
       
      }
    })
  }

  selectRating(event: any) {
    this.myrating = event.target.value
    const params = {
      catagory: "",
      pagno: 0,
      sort: 1,
      pricerangeLow: "",
      pricerangeHigh: "",
      rating: this.myrating,
      condition: ""

    }

    this.http.post( this.allproductsApi,params).subscribe(res=>{
      this.navURLRes = res
      // console.log(this.navURLRes)
      this.allSearchProducts =  this.navURLRes.data.products
      
     
    })

  }

  selectprice(event:any){
    this.minval= event.target.id
    this.maxval = event.target.value

    const params = {
      catagory: "",
      pagno: 0,
      sort: 1,
      pricerangeLow: this.minval,
      pricerangeHigh: this.maxval,
      rating: "",
      condition: ""

    }
    console.log(params)
    this.http.post( this.allproductsApi,params).subscribe(res=>{
      this.navURLRes = res
       
      this.allSearchProducts =  this.navURLRes.data.products
      
     
    })

  }

  selectcondition(event: any) {
    this.condition = event.target.value
    const params = {
      catagory: "",
      pagno: 0,
      sort: 1,
      pricerangeLow: "",
      pricerangeHigh: "",
      rating: "",
      condition: this.myrating

    }

    this.http.post( this.allproductsApi,params).subscribe(res=>{
      this.navURLRes = res
      // console.log(this.navURLRes)
      this.allSearchProducts =  this.navURLRes.data.products
     
     
    })

  }

}
