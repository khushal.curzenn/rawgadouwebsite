import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserServiceService } from 'src/app/user-service.service';
import { Router, ActivatedRoute } from '@angular/router';

import * as $ from 'jquery'
@Component({
  selector: 'app-eventlist',
  templateUrl: './eventlist.component.html',
  styleUrls: ['./eventlist.component.css']
})
export class EventlistComponent implements OnInit {
  records: any;
  p: any;
  searchText: any;
  showerror = true;
  baseurl: any
  eventURL: any; eventURLRes: any
  user_id: any
  myOptions: any
  showcatagories: any; showcatagoriesRes: any; myCategories: any
  savefavriout: any; savefavrioutRes: any

  getRandombannerbyid: any; getRandombannerbyidRes: any; bannerImg: any
  deletefavoriteApi:any; deletefavoriteApires:any
  myrouter:any
  constructor(private http: HttpClient, private user: UserServiceService,
    private router: Router) {
    this.baseurl = user.baseapiurl2
    this.user_id = localStorage.getItem('main_userid')
    this.eventURL = this.baseurl + "api/event/getallevents_without_pagination"
    this.showcatagories = this.baseurl + "api/event/showcatagories"
    this.savefavriout = this.baseurl + "api/event/savefavriout"
    this.getRandombannerbyid = this.baseurl + "api/event/getRandombannerbyid"
    this.deletefavoriteApi = this.baseurl + "api/event/deletefavorite"
    
    this.records = []
    this.myOptions = {
      user_id: this.user_id,
      latlong: '',
      radius: '',
      catagory: '',
      start_date: '',
      end_date: '',
      paidornot: ''
    }
    this.myrouter = ''
    
  }

  ngOnInit(): void {
    // this.http.get('').subscribe(res=>{
    //   this.records = res
    // })
    this.myrouter = this.router.url

   
    this.http.post(this.eventURL, this.myOptions).subscribe(res => {
      this.eventURLRes = res
      this.records = this.eventURLRes.data
      
    })
    const parms = {
      "pageno": 0
    }

    this.http.post(this.showcatagories, parms).subscribe(res => {
      this.showcatagoriesRes = res
      if (this.showcatagoriesRes.status) {
        this.myCategories = this.showcatagoriesRes.data
       
      }


    })

    this.http.get(this.getRandombannerbyid).subscribe(res=>{
      this.getRandombannerbyidRes =  res
      if(this.getRandombannerbyidRes.status){
        this.bannerImg = this.getRandombannerbyidRes.data
        
      }
    })

  }

  addFav(id: any) {

    const params = {
      "user_id": this.user_id,
      "event_id": id
    }
    this.http.post(this.savefavriout, params).subscribe(res => {
      this.savefavrioutRes = res
      if (this.savefavrioutRes.status) {
        window.location.reload()
      }
    })

  }
  removeFav(id:any){
    console.log(id)
    const params={
      "user_id": this.user_id ,
      "event_id":id
    }
    this.http.post(this.deletefavoriteApi, params).subscribe(res=>{
      this.deletefavoriteApires = res
      if(this.deletefavoriteApires.status){
        window.location.reload()
      }
    })
  }

  filterCategories(cat: any) {


    this.myOptions.catagory = cat

    this.http.post(this.eventURL, this.myOptions).subscribe(res => {
      this.eventURLRes = res
      this.records = this.eventURLRes.data
      
    })
  }

  All() {
    this.myOptions.catagory = ""

    this.http.post(this.eventURL, this.myOptions).subscribe(res => {
      this.eventURLRes = res
      this.records = this.eventURLRes.data
      
    })
  }













  checkClass = () => {
    this.showerror = true;
    if ($('.item').hasClass('hide')) {
      $('.item').removeClass('hide');
    }

  };


  Arts() {
    this.checkClass();
    $('.item:not(.Arts)').toggleClass('hide');
    var error = $('.item:is(.Arts)').length
    if (error == 0) {
      this.showerror = false;
    } else {
      this.showerror = true;
    }
  }
  Musique() {
    this.checkClass();
    $('.item:not(.Musique)').toggleClass('hide');
    var error = $('.item:is(.Musique)').length
    if (error == 0) {
      this.showerror = false;
    } else {
      this.showerror = true;
    }
  }
  Voyages() {
    this.checkClass();
    $('.item:not(.Voyages)').toggleClass('hide');
    var error = $('.item:is(.Voyages)').length
    if (error == 0) {
      this.showerror = false;
    } else {
      this.showerror = true;
    }
  }
  Sante() {
    this.checkClass();
    $('.item:not(.Sante)').toggleClass('hide');
    var error = $('.item:is(.Sante)').length
    if (error == 0) {
      this.showerror = false;
    } else {
      this.showerror = true;
    }

  }
  Loisirs() {
    this.checkClass();
    $('.item:not(.Loisirs)').toggleClass('hide');
    var error = $('.item:is(.Loisirs)').length
    if (error == 0) {
      this.showerror = false;
    } else {
      this.showerror = true;
    }
  }
  Affaires() {

    this.checkClass();
    $('.item:not(.Affaires)').toggleClass('hide');
    var error = $('.item:is(.Affaires)').length
    if (error == 0) {
      this.showerror = false;
    } else {
      this.showerror = true;
    }
  }
  Gastronomie() {
    this.checkClass();
    $('.item:not(.Gastronomie)').toggleClass('hide');
    var error = $('.item:is(.Gastronomie)').length
    if (error == 0) {
      this.showerror = false;
    } else {
      this.showerror = true;
    }

  }
  Sports() {
    this.checkClass();
    $('.item:not(.Sports)').toggleClass('hide');
    var error = $('.item:is(.Sports)').length
    if (error == 0) {
      this.showerror = false;
    } else {
      this.showerror = true;
    }
  }
  openEvent(event: any) {
    console.log(event)
  }
}
