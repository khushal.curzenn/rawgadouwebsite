import { Component, HostListener, Input, OnInit } from '@angular/core';
import { ViewportScroller } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { UserServiceService } from '../user-service.service';
import { Slick } from 'ngx-slickjs';
//import { map, takeWhile, timer } from 'rxjs';
import { interval, Observable } from "rxjs";
import { map, shareReplay } from "rxjs/operators";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  // @Input() seconds = 300;

  // timeRemaining$ = timer(0, 1000).pipe(
  //   map(n => (this.seconds - n) * 1000),
  //   takeWhile(n => n >= 0),
  // );
  baseurl: any; bannerURL: any; bannerData: any
  topCategoriesURL: any; topCategoriesData: any
  feauturedCategoriesURL: any; feauturedCategoriesData: any
  firstBannerImg: any
  addFavProduct: any; addFavProductRes: any
  homepro: any
  user_id: any
  deleteFavProduct: any; deleteFavProductRes: any
  getMiddlebannerimages: any; smallBanners: any
  middlebanner: any
  mydate:any

  days:any; hours:any; mins:any; seconds:any

  showTime: any;
  public timerInterval: any;
  timeLeft$:any;
  //public timeLeft$: Observable<timeComponents>;

  topProducts:any; tProducts:any; topProductsres:any;
  constructor(private viewportScroller: ViewportScroller, private http: HttpClient, private user: UserServiceService,) {
    this.baseurl = user.baseapiurl2
    this.user_id = localStorage.getItem('main_userid')
    this.bannerURL = this.baseurl + "api/admin/getallbannerimage"
    this.topCategoriesURL = this.baseurl + "api/admin/gettopcatagories"
    this.feauturedCategoriesURL = this.baseurl + "api/admin/getFeatruedProductcatagory"
    this.addFavProduct = this.baseurl + "api/product/addFavProduct"
    this.deleteFavProduct = this.baseurl + "api/product/deleteFavProduct/"
    this.getMiddlebannerimages = this.baseurl + "api/admin/getMiddlebannerimages"

    this.topProducts = this.baseurl + "api/product/topProducts"
    this.homepro = []
    
    // this.timeLeft$ = interval(1000).pipe(
    //   map(x => this.calcDateDiff()),
    //   shareReplay(1)
      
    // );
     
  }
  //calcDateDiff(endDay: Date = new Date(2023, 9, 13)  )
  calcDateDiff(endDay: any )
  {
    //console.log("today date ", new Date() );
    //  endDay = new Date( endDay.setHours(12, 44, 53))
      endDay = new Date(endDay)
    //console.log("endDay ", endDay);
    const dDay = endDay.valueOf();
    

    const milliSecondsInASecond = 1000;
    const hoursInADay = 24;
    const minutesInAnHour = 60;
    const secondsInAMinute = 60;
  
    const timeDifference = dDay - Date.now();
  
    const daysToDday = Math.floor(
      timeDifference /
        (milliSecondsInASecond * minutesInAnHour * secondsInAMinute * hoursInADay)
    );
  
    const hoursToDday = Math.floor(
      (timeDifference /
        (milliSecondsInASecond * minutesInAnHour * secondsInAMinute)) %
        hoursInADay
    );
  
    const minutesToDday = Math.floor(
      (timeDifference / (milliSecondsInASecond * minutesInAnHour)) %
        secondsInAMinute
    );
      //console.log("minutesToDday ", minutesToDday);
    const secondsToDday =
      Math.floor(timeDifference / milliSecondsInASecond) % secondsInAMinute;
  
    return { secondsToDday, minutesToDday, hoursToDday, daysToDday };
  }

  checkStartDate(start_date:any)
  {
    console.log("start_date ", start_date);
    let date1 = new Date(start_date).getTime();
    let date2 = new Date().getTime();
    if (date1 <= date2) 
    {
        console.log("return true");
        return true;
    }else{
      console.log("return false");
      return false;
    }
     
  } 
   
  arrayLength = 10;

  config: Slick.Config = {
    infinite: true,
    slidesToShow: 5,
    slidesToScroll: 2,
    dots: false,
    autoplay: true,
    autoplaySpeed: 2000,
    accessibility: false,
    responsive: [

      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 1008,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 900,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 700,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 400,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },

    ],
    nextArrow: "<button class='btn ' style='position: absolute !important;right: 0 !important;top: 50% !important;z-index: 2 !important;background: #eaeded; color: rgba(0, 0, 0, 0.4);height: 75px; border: none !important; width: 50px;margin: 0;border-radius: 0;outline: none !important;box-shadow: none !important;'><i class='fa fa-angle-right' styele='font-size:5rem !important'></i></button>",
    prevArrow: "<button  class='btn '  style='position: absolute !important;left: 0 !important;top: 50% !important;z-index: 2 !important;background: #eaeded; color: rgba(0, 0, 0, 0.4);height: 75px; border: none !important; width: 50px;margin: 0;border-radius: 0;outline: none !important;box-shadow: none !important;'><i class='fa fa-angle-left' styele='font-size:5rem !important'></i></button>",
  }

  getArray(count: number) {
    return new Array(count)
  }

  ngOnInit(): void {
    this.getFeaturedCategories()
    this.getMiddleBanner()
    this.convertTime();
   this.getTopProducts()
    
  }

  getFeaturedCategories() {
    const params = {
      "user_id": this.user_id
    }
    this.http.post(this.feauturedCategoriesURL, params).subscribe(res => {
      this.feauturedCategoriesData = res

      if (this.feauturedCategoriesData.status) {
        this.homepro = this.feauturedCategoriesData.data
        console.log("homepro ", this.homepro)
      }



    })
  }
  addFavItem(productid: any) {

    const parms = {
      "product_id": productid,
      "user_id": this.user_id
    }

    this.http.post(this.addFavProduct, parms).subscribe(res => {
      this.addFavProductRes = res

      if (this.addFavProductRes.status) {
        window.location.reload()
      }
    })

  }
  removeFavItem(productid: any) {

    this.http.get(this.deleteFavProduct + productid).subscribe(res => {
      this.deleteFavProductRes = res
      console.log(res)

      if (this.deleteFavProductRes.status) {
        window.location.reload()
      }
    })
  }

  getMiddleBanner() {
    this.http.get(this.getMiddlebannerimages).subscribe(res => {
      this.smallBanners = res
      this.middlebanner = this.smallBanners.data

      console.log(this.middlebanner, "dfhdfsudjheujh")

    })
  }

  convertTime(){
    const countDownDate = new Date("oct 30, 2023 15:37:25").getTime();

    setInterval(() => {
      const now = new Date().getTime();
      const distance = countDownDate - now;



     this.mydate = new Date()
      this.days = Math.floor(distance / (1000 * 60 * 60 * 24))
     this.hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
     this.seconds = Math.floor((distance % (1000 * 60)) / 1000);
      this.mins = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    
    }, 1000);
  }

  getcount(startdate: any, enddate: any){
    

    // const countDownDate = new Date(startdate).getTime();
    // setInterval(() => {
    //   const now = enddate;
    //   const distance = countDownDate - now;



    //  this.mydate = new Date()
    //   this.days = Math.floor(distance / (1000 * 60 * 60 * 24))
    //  this.hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    //  this.seconds = Math.floor((distance % (1000 * 60)) / 1000);
    //   this.mins = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    
    // }, 1000);

    // // var abc =  startdate + enddate
    // // return abc
  }




  getTopProducts(){
    this.http.post(this.topProducts , {}).subscribe(res=>{
      this.topProductsres = res
      if(this.topProductsres.status){
        this.tProducts =  this.topProductsres.data
      }
      console.log(res, "rajesh")
    })
  }





}
