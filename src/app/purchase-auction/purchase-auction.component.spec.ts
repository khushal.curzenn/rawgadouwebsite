import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchaseAuctionComponent } from './purchase-auction.component';

describe('PurchaseAuctionComponent', () => {
  let component: PurchaseAuctionComponent;
  let fixture: ComponentFixture<PurchaseAuctionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PurchaseAuctionComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PurchaseAuctionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
