import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MusiqueEtDvdComponent } from './musique-et-dvd.component';

describe('MusiqueEtDvdComponent', () => {
  let component: MusiqueEtDvdComponent;
  let fixture: ComponentFixture<MusiqueEtDvdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MusiqueEtDvdComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MusiqueEtDvdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
