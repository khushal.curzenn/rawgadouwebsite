import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserServiceService } from '../user-service.service';
@Component({
  selector: 'app-event-cart',
  templateUrl: './event-cart.component.html',
  styleUrls: ['./event-cart.component.css']
})
export class EventCartComponent implements OnInit {
  baseurl: any; user_id: any
  getcartbyuserid: any; apiRes: any; allCart: any; totalprice: any; mylen: any
  deleteEventfromcart: any; deleteEventfromcartRes: any
  eventcartcheckout: any; eventcartcheckoutRes: any
  incEventtocartWeb:any; incEventtocartWebRes:any
  all_available:any; totalItem:number=0;
  constructor(private http: HttpClient, private user: UserServiceService) {
    this.baseurl = user.baseapiurl2
    this.user_id = localStorage.getItem('main_userid')
    this.getcartbyuserid = this.baseurl + "api/event/getcartbyuserid"
    this.deleteEventfromcart = this.baseurl + "api/event/deleteEventfromcart"
    this.eventcartcheckout = this.baseurl + "api/event/eventcartcheckout"
    this.incEventtocartWeb = this.baseurl + "api/event/incEventtocartWeb"
    this.mylen = ''
    this.all_available =[]
  }

  ngOnInit(): void {
   
    const params = {
      "userid": this.user_id
    }
    this.http.post(this.getcartbyuserid, params).subscribe(res => {
      this.apiRes = res
      if (this.apiRes.status) {
        this.totalprice = this.apiRes.totalprice
        this.allCart = this.apiRes.data.cart;

        console.log("this.allCart " , this.allCart);
        for(let c=0; c<this.allCart.length; c++)
        {
          this.totalItem = this.totalItem + this.allCart[c].quantity;
        }

        this.mylen = this.allCart.length
        this.all_available = this.apiRes.all_available;

        console.log(this.apiRes, "event")
      }
    })
  }

  deleteitem(id: any) {
    const confirmation = confirm("Souhaitez-vous supprimer cet article du panier ?")
    if (confirmation) {
      const params = {
        "userid": this.user_id,
        "eventid": id
      }
      this.http.post(this.deleteEventfromcart, params).subscribe(res => {
        this.deleteEventfromcartRes = res
        if (this.deleteEventfromcartRes.status) {
          window.location.reload()
        }
      })
    }

  }
  updateQTY(eve: any, id: any) {
    const params = {
      "userid": this.user_id,
      "eventid": id,
      "quantity": eve.target.value
    }

    this.http.post(this.incEventtocartWeb, params).subscribe(res=>{
      this.incEventtocartWebRes = res
      if(this.incEventtocartWebRes.status){
        window.location.reload()
      }
    })

  }

  checkoutCart() {
    const params = {
      "user_id": this.user_id
    }
    console.log( params)
    this.http.post(this.eventcartcheckout, params).subscribe(res => {
      this.eventcartcheckoutRes = res
      console.log( this.eventcartcheckoutRes)

      if( this.eventcartcheckoutRes.status){
        if(this.eventcartcheckoutRes.url != ''){
          window.location.href = this.eventcartcheckoutRes.url
        }else{
           window.location.href="/event_confirm"
        }
       
      }
    })


  }
  numSequence(n: number): Array<number> {
    return Array(n);
  }

}
