import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { SecurityComponent } from '../security/security.component';
import { UserServiceService } from '../user-service.service';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  baseurl:any; useremail:any
  createuser:any; createuserRes:any
  myform!:FormGroup;confirm:any
  constructor(private seller: UserServiceService, private http: HttpClient,) {
    this.baseurl = seller.baseapiurl2
    this.useremail = localStorage.getItem('useremail')
    this.createuser = this.baseurl + "api/user/createuser"
    this.confirm = true
    this.myform = new FormGroup({
      password: new FormControl('', Validators.required),
      newpassword: new FormControl('', Validators.required),
      confirmpassword: new FormControl('', Validators.required)
     
    })
   }

  ngOnInit(): void {
  }

  send(){
    this.myform.value.email =  this.useremail
    if(this.myform.valid){
      if(this.myform.value.newpassword == this.myform.value.confirmpassword){
        this.http.post(this.createuser, this.myform.value).subscribe(res=>{
          this.createuserRes = res
    
          if(this.createuserRes.status){
            window.location.href="/login"
          }
        })
      }else{
        this.confirm = false
      }
     
    }
    else{
      for (const control of Object.keys(this.myform.controls)) {
        this.myform.controls[control].markAsTouched();
      }
      return;
    }
  }

}
