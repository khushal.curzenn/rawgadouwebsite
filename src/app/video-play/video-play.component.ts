import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ViewProductComponent } from '../view-product/view-product.component';
import { UserServiceService } from '../user-service.service';

@Component({
  selector: 'app-video-play',
  templateUrl: './video-play.component.html',
  styleUrls: ['./video-play.component.css']
})
export class VideoPlayComponent implements OnInit {
  baseurl:any;my_video:any
  myData:any
  constructor(@Inject(MAT_DIALOG_DATA) public data: ViewProductComponent,
  private seller: UserServiceService,) { 
    this.baseurl = seller.baseapiurl2
    this.myData = this.data
  }

  ngOnInit(): void {
    console.log( this.myData)
  }

}
