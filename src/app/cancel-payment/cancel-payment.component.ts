import { Component, OnInit } from '@angular/core';

import { UserServiceService } from '../user-service.service';
@Component({
  selector: 'app-cancel-payment',
  templateUrl: './cancel-payment.component.html',
  styleUrls: ['./cancel-payment.component.css']
})
export class CancelPaymentComponent implements OnInit {
  base_url: any; 
  constructor( private user: UserServiceService) {
    this.base_url = user.baseapiurl2
   }

  ngOnInit(): void {
  }

}
