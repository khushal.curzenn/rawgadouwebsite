import { HttpClient } from '@angular/common/http';
import { Component, OnInit,HostListener } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { UserServiceService } from '../user-service.service';
import { Router } from '@angular/router';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { AddAddressComponent } from '../add-address/add-address.component';
import { DOCUMENT } from '@angular/common';
@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {
  CheckoutProducts: any; addressform!: FormGroup; addressform_shipping!: FormGroup;
  myAddressForm: any; user_email: any; baseurl: any; addressURL: any
  fulldata: any; getAddressURL: any; user_id: any
  myaddressArray: any; shippingURL: any; shippingURLres: any
  deleteaddressURL: any; beforeCheckeditem: any
  beforeCheckingURL: any; checkingoptions: any
  checkingitmes: any; checkingproducts: any; shippingCity: any;shippingAddress:any;
  checkoutURL: any; checkoutOptions: any; checkoutApires: any
  cartURL: any; cartOptions: any
  promoForm!: FormGroup

  globalChekArr: any
  showMymsg: any;
   showMymsg2:boolean = true;
  all_available:boolean=true;
  checkpromocode: any; checkpromocodeRes: any;classActive:string=""; classInActive:string="active";
  gettedItems:any; deliveryDefaultAddress:number=0;apiResponse:any;
  cash_on_delivery_var:number = 0;
  constructor(private user: UserServiceService, private http: HttpClient,
    private router: Router, private dialog: MatDialog,) {
      this.gettedItems = [];
    this.CheckoutProducts = []
    this.myaddressArray = []
    this.beforeCheckeditem = []
    this.checkingitmes = []
    this.checkingproducts = []
    this.baseurl = user.baseapiurl2
    this.cartURL = this.baseurl + "api/cart/getcartbyuserid";
    this.addressURL = this.baseurl + "api/user/createuser";
    this.getAddressURL = this.baseurl + "api/user/getuserprofile/";
    this.shippingURL = this.baseurl + "api/user/markaddressasshippingaddress";
    this.deleteaddressURL = this.baseurl + "api/user/deleteaddress";
    this.beforeCheckingURL = this.baseurl + "api/cart/beforecheckoutcart";
    this.checkoutURL = this.baseurl + "api/cart/checkoutcart";
    this.checkpromocode = this.baseurl + "api/cart/checkpromocode/";
    this.globalChekArr = []
    this.showMymsg = true



    this.user_email = localStorage.getItem("useremail")
    this.user_id = localStorage.getItem("main_userid")
    this.myAddressForm = {
      email: this.user_email
    }
    this.addressform = new FormGroup({
      country: new FormControl(''),
      name: new FormControl(''),
      pobox: new FormControl(''),
      telephone: new FormControl(''),
      address: new FormControl(''),
      city: new FormControl(''), 

    })


    this.addressform_shipping = new FormGroup({ 
      shipping_type: new FormControl(''),

    })

    this.promoForm = new FormGroup({

      promocode: new FormControl(''),

    })
    this.checkingoptions = {
      id: this.user_id,
      //"shipping_city":"Andra",
      from: "cart",
      code: "",
      shipping_id: [
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        ""
      ]
    }
    this.checkoutOptions = {
      id: this.user_id,
      // shipping_city: this.shippingCity,
      from: "cart",
      code: "",
      shipping_id: [
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        ""
      ]
    }
  }

  ngOnInit(): void {
    this.getAddress()
    this.http.post(this.cartURL, { userid:this.user_id }).subscribe(res => {
      this.gettedItems = res;
      //console.log("cart item ",this.gettedItems );
      if(this.gettedItems.status == true)
      {
        this.all_available = this.gettedItems.all_available;
      }
      
      
    })
  }

  getAddress() {
    this.http.get(this.getAddressURL + this.user_id).subscribe(res => {
      this.CheckoutProducts = res
      this.myaddressArray = this.CheckoutProducts.data
      //console.log(this.myaddressArray)
      const getcheckeditem = this.myaddressArray.address.find((x: any) => {
        
        return x.is_shipping_address == true
      })


      console.log("getcheckeditem ", getcheckeditem);


      if (getcheckeditem) {
        console.log("iffffffffffffffffffffff 136  jdsfdf ");
        this.deliveryDefaultAddress = 1;
        this.beforeCheckeditem = getcheckeditem
        
        this.checkingoptions.shipping_city = this.beforeCheckeditem.city
        this.shippingCity = this.beforeCheckeditem.city
        this.shippingAddress = this.beforeCheckeditem.address.address;


        this.http.post(this.beforeCheckingURL, this.checkingoptions).subscribe(res => {
          console.log("res ",res);
          this.checkingitmes = res;
          this.checkingproducts = this.checkingitmes.data
          console.log( "chek" , this.checkingproducts)

        })
      } 
    })
  }

  checkout() {

    if(this.cash_on_delivery_var == 0)
    {
      this.showMymsg2 = false;
      console.log("hereeeee");
      console.log("this.showMymsg2 ", this.showMymsg2);
    }else{
      this.showMymsg2 = true;
      this.checkoutOptions.shipping_city = this.shippingCity
      this.checkoutOptions.shipping_id = this.globalChekArr;
      this.checkoutOptions.cash_on_delivery_var = this.cash_on_delivery_var;
      console.log(this.checkoutOptions)
      this.http.post(this.checkoutURL, this.checkoutOptions).subscribe(res => {
        this.checkoutApires = res
        if (this.checkoutApires.status == true)
        {
          this.showMymsg = true
          console.log("hereeeeeeee 164")
          console.log(this.checkoutApires)

          window.open(this.checkoutApires.url, '_self');
          
        } else {
          //this.showMymsg = false
          this.showMymsg = true
        }
      })
    }
    
  }


  addAddress() {
    this.myAddressForm.address = this.addressform.value
    // console.log(this.myAddressForm)

    this.http.post(this.addressURL, this.myAddressForm).subscribe(res => {
      this.fulldata = res
      //   console.log(this.fulldata)
      if (this.fulldata.status == true) {
        alert("address added success")
      } else {
        alert(this.fulldata.errmessage)
      }
    })
  }

  openDialog() {
    this.dialog.open(AddAddressComponent, {
      height: '650px',
      width: '600px',
    });
  }

  shipAddress(id: any) {
    this.myAddressForm.addressid = id
    //  console.log(this.myAddressForm)
    // const conf = confirm("Voulez-vous définir cette adresse comme adresse de livraison ?")
    // if (conf == true) {
      this.http.post(this.shippingURL, this.myAddressForm).subscribe(res => {
        this.shippingURLres = res

        if (this.shippingURLres.status == true) {
 
          window.location.reload()
        }
      })
   
  }

  deleteAddress(id: any) {
    this.myAddressForm.addressid = id
    // console.log(this.myAddressForm)
    const conf = confirm("Voulez-vous supprimer l'adresse ?")
    if (conf == true) {
      this.http.post(this.deleteaddressURL, this.myAddressForm).subscribe(res => {
        this.shippingURLres = res
        if (this.shippingURLres.status == true) {
          alert(this.shippingURLres.message)
          window.location.reload()
        }
      })
    } 
  }

  applyPromo() {

    this.http.get(this.checkpromocode + this.promoForm.value.promocode).subscribe(res => {
      this.checkpromocodeRes = res
      if (this.checkpromocodeRes.status) {

        const checkingoptionsPromo = {
          id: this.user_id,
          shipping_city: this.checkingoptions.shipping_city,
          from: "cart",
          code: this.promoForm.value.promocode,
          shipping_id: this.globalChekArr
        }
        console.log(checkingoptionsPromo)

        this.http.post(this.beforeCheckingURL, checkingoptionsPromo).subscribe(res => {

          this.checkingitmes = res
          this.checkingproducts = this.checkingitmes.data

        })
      }
    })



  }

  deliveryMethid(id: any, length: any, index: any) {
    console.log("hereeeeeeeeeeeeee 254")
    console.log(this.globalChekArr)
    const arr = []
    for (var i = 0; i < length; i++) {
      // arr.push("")
      this.globalChekArr.push("")

    }

    this.globalChekArr.splice(index, 1, id);

    this.checkingoptions.shipping_id = this.globalChekArr;
    this.checkingoptions.code = this.promoForm.value.promocode;
    console.log("this.checkingoptions" ,this.checkingoptions)

    this.http.post(this.beforeCheckingURL, this.checkingoptions).subscribe(res => {

      this.checkingitmes = res
      this.checkingproducts = this.checkingitmes.data

    })

  }
  //@HostListener('window:scroll')
  openCheckout()
  {
    console.log("click checkout");
    if(this.deliveryDefaultAddress == 0)
    {

      this.apiResponse = {error:true,"errorMessage":"Veuillez sélectionner la première adresse de livraison"};
    }else{

      // (function smoothscroll() {

      //   var currentScroll = document.documentElement.scrollTop || document.body.scrollTop;

      //   if (currentScroll > 0) {

      //       window.requestAnimationFrame(smoothscroll);

      //       window.scrollTo(0, currentScroll - (currentScroll / 8));

      //   }

      // })();

      this.classInActive = "";
      this.classActive = "active";
      
      
      console.log("continue");
    }
  }
  reverseFun()
  {
    this.classInActive = "active";
      this.classActive = "";
  }
  cash_on_delivery()
  {
    this.cash_on_delivery_var = 1;
    this.showMymsg2 = true;
    console.log("cash on delivery");
  }
  mobile_pay(typee:any)
  {
    this.cash_on_delivery_var = typee;
    console.log("this.cash_on_delivery_var ", this.cash_on_delivery_var);
    this.showMymsg2 = true;
    console.log("cash on delivery");
  }
  
}
