import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { UserServiceService } from '../user-service.service';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
  changePassword!: FormGroup
  changetype: boolean = false;
  visible: boolean = true;
  confirm = true;
  validpassword = false;
  confirmed = true
  baseurl: any
  fulldata: any
  submitted = false;
  showtandc: any; resetID: any; Finalregisterform: any; forgetpassAPI: any
  Apiresposnse: any; myoptions: any
  constructor(private activateroute: ActivatedRoute,
    private seller: UserServiceService, private http: HttpClient) {
      this.baseurl = seller.baseapiurl2
    this.resetID = this.activateroute.snapshot.params['id']
    this.forgetpassAPI = this.baseurl + "api/user/userSubmitForgotPassword"
    this.myoptions = {
      id: this.resetID
    }
     }

  ngOnInit(): void {
    this.changePassword = new FormGroup({
      password: new FormControl('', [Validators.required, Validators.minLength(8)]),
      password2: new FormControl('', Validators.required),
    })
  }
  reset() {
    this.myoptions.new_password = this.changePassword.value.password

    // this.Finalregisterform = new FormData();
    // this.Finalregisterform.append('id', this.resetID);
    // this.Finalregisterform.append('new_password', this.changePassword.value.password);
    // console.log(this.myoptions) 

    if (this.changePassword.valid) {
      if (this.changePassword.value.password == this.changePassword.value.password2) {
        this.http.post(this.forgetpassAPI, this.myoptions).subscribe(res => {
          this.Apiresposnse = res
          this.changePassword.reset()
          console.log(this.Apiresposnse)
          // if (this.Apiresposnse.status == true) {
          //   window.location.href = "/login"
          // }

        })
      }

    } else {
      for (const control of Object.keys(this.changePassword.controls)) {
        this.changePassword.controls[control].markAsTouched();
      }
      return;
    }

  }
  confirmpassword() {
    if (this.changePassword.value.password != this.changePassword.value.password2) {
      //console.log("fine")
      this.confirm = false;
      this.confirmed = true
    } else {
      this.confirm = true;
      this.confirmed = false

    }
  }
  viewpass() {
    this.visible = !this.visible;
    this.changetype = !this.changetype;
  }

}
