import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MessegesChatComponent } from './messeges-chat.component';

describe('MessegesChatComponent', () => {
  let component: MessegesChatComponent;
  let fixture: ComponentFixture<MessegesChatComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MessegesChatComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MessegesChatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
