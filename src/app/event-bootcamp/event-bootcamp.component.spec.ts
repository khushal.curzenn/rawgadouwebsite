import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EventBootcampComponent } from './event-bootcamp.component';

describe('EventBootcampComponent', () => {
  let component: EventBootcampComponent;
  let fixture: ComponentFixture<EventBootcampComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EventBootcampComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EventBootcampComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
