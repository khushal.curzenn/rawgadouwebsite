import { Component, OnInit } from '@angular/core';
import { UserServiceService } from '../user-service.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-termsandconditions',
  templateUrl: './termsandconditions.component.html',
  styleUrls: ['./termsandconditions.component.css']
})
export class TermsandconditionsComponent implements OnInit {
  baseurl:any
  user_web_terms_condition:any; user_web_terms_conditionRes:any
  myRec:any; termsPage:any
  constructor(private seller: UserServiceService, private http: HttpClient,) {
    this.baseurl = seller.baseapiurl2
    this.user_web_terms_condition = this.baseurl+"api/getWebPageBySlug/user_web_terms_condition"
   }

  ngOnInit(): void {
    this.http.get(this.user_web_terms_condition).subscribe(res=>{
      this.user_web_terms_conditionRes = res
      if(this.user_web_terms_conditionRes.success){
        this.myRec = this.user_web_terms_conditionRes.data 
        this.termsPage = this.myRec.description
        console.log(this.termsPage, "hggh")
      }
    })
  }
}
