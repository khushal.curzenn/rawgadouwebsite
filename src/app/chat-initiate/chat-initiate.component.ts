import { Component, OnInit } from '@angular/core';
import { Route, ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { UserServiceService } from '../user-service.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
@Component({
  selector: 'app-chat-initiate',
  templateUrl: './chat-initiate.component.html',
  styleUrls: ['./chat-initiate.component.css']
})
export class ChatInitiateComponent implements OnInit {

  baseurl: any; user_id: any; product_id: any
  productURL: any; productURLRes: any
  myproduct: any; sellerId:any
  messageform!: FormGroup
  new_initiate_chat:any; new_initiate_chatRes:any

  constructor(private ActiveRoute: ActivatedRoute, private http: HttpClient,
    private user: UserServiceService) {
    this.baseurl = user.baseapiurl2
    this.user_id = localStorage.getItem('main_userid')
    this.product_id = this.ActiveRoute.snapshot.params['id']

    this.productURL = this.baseurl + "api/product/getallproducts"
    this.new_initiate_chat = this.baseurl + "api/chat/new_initiate_chat"
    this.messageform = new FormGroup({
      message: new FormControl('', Validators.required)

    })

  }

  ngOnInit(): void {
    const params = {
      "singleid": this.product_id
    }
    this.http.post(this.productURL, params).subscribe(res => {
      this.productURLRes = res
      if (this.productURLRes.status) {
        this.myproduct = this.productURLRes.data[0]
        this.sellerId = this.myproduct.selr_id
        console.log(this.myproduct)
      }

    })
  }
  sendMsg() {
    const ChatParams = {
      "product_id": this.product_id,
      "seller_id":  this.sellerId,
      "user_id":  this.user_id,
      "message": this.messageform.value.message,
      "message_by": 2
    }
    if (this.messageform.valid) {

      this.http.post(this.new_initiate_chat, ChatParams).subscribe(res=>{
        this.new_initiate_chatRes = res
        if(this.new_initiate_chatRes.status){
          this.messageform.reset()
        }
      })
     
    } else {
      for (const control of Object.keys(this.messageform.controls)) {
        this.messageform.controls[control].markAsTouched();
      }
      return;
    }

  }

}
