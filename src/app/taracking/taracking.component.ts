import { Component, OnInit } from '@angular/core';
import { UserServiceService } from '../user-service.service';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-taracking',
  templateUrl: './taracking.component.html',
  styleUrls: ['./taracking.component.css']
})
export class TarackingComponent implements OnInit {
  trackingid: any; baseurl: any
  getTrackingDetails: any; getTrackingDetailsRes: any
  trackRec: any; trackRec2:any
  order_id:number = 0;
  orderdate: any
  readytopickup: any
  delivery_status: any
  delivery_date: any
  constructor(private seller: UserServiceService, private http: HttpClient,
    private router: Router, private activateroute: ActivatedRoute) {
    this.trackingid = this.activateroute.snapshot.params['id'];
    this.order_id = this.activateroute.snapshot.params['part_id'];
    this.baseurl = seller.baseapiurl2

    this.getTrackingDetails = this.baseurl + "api/order/getTrackingDetails/" + this.trackingid+"/"+this.order_id;


    this.orderdate = ''
    this.readytopickup = ''
    this.delivery_status = ''
    this.delivery_date = ''



    this.getVal();

    setInterval(()=>{
      //console.log("hereeee");
      this.http.get(this.getTrackingDetails).subscribe(res => {
        this.getTrackingDetailsRes = res
  
        if (this.getTrackingDetailsRes.status) 
        {
  
          console.log("this.getTrackingDetailsRes.data",this.getTrackingDetailsRes.data);
  
          this.trackRec = this.getTrackingDetailsRes.data[0];

          //console.log("this.trackRec ", this.trackRec);


          this.trackRec2 = this.getTrackingDetailsRes.data[0].products[0]
          this.orderdate = this.trackRec.date_of_transaction
          this.readytopickup = this.trackRec2.readytopickup
          this.delivery_status = this.trackRec2.delivery_status
          this.delivery_date = this.trackRec.date_of_delivery
        }
  
  
      })
    },1000);


  }

  ngOnInit(): void {

   
  }

  getVal()
  {
    
  }
}
