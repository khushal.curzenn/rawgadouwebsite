import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TarackingComponent } from './taracking.component';

describe('TarackingComponent', () => {
  let component: TarackingComponent;
  let fixture: ComponentFixture<TarackingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TarackingComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TarackingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
