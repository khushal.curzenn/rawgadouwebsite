import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserServiceService } from '../user-service.service';
@Component({
  selector: 'app-event-confirm',
  templateUrl: './event-confirm.component.html',
  styleUrls: ['./event-confirm.component.css']
})
export class EventConfirmComponent implements OnInit {
  clearcart:any; clearcartRes:any
  baseurl:any; user_id:any
  constructor(private http: HttpClient, private user: UserServiceService) {
    this.baseurl = user.baseapiurl2
    this.user_id = localStorage.getItem('main_userid')
    this.clearcart = this.baseurl + "api/event/clearcart"
   }

  ngOnInit(): void {
    const params = {
      "userid": this.user_id
    }

    this.http.post(this.clearcart, params).subscribe(res=>{
      
    })
  }

}
