import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ViewProductComponent } from '../view-product/view-product.component';
import { UserServiceService } from '../user-service.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-abuse-report',
  templateUrl: './abuse-report.component.html',
  styleUrls: ['./abuse-report.component.css']
})
export class AbuseReportComponent implements OnInit {
  baseurl:any;my_video:any
  myData:any
  main_userid: any
  createuser: any; createuserRes: any
  myform!: FormGroup
  constructor(@Inject(MAT_DIALOG_DATA) public data: ViewProductComponent,
  private seller: UserServiceService,private http: HttpClient) { 
    this.baseurl = seller.baseapiurl2;
    this.main_userid = localStorage.getItem('main_userid');

    this.myData = this.data;

    this.createuser = this.baseurl + "api/user/createuser"

    this.myform = new FormGroup({
      productId: new FormControl(this.myData.ad_id, ),
      reportedbyId: new FormControl(this.main_userid, ),
      reason: new FormControl("", Validators.required)
    })


  }

  ngOnInit(): void {
    console.log("abuse report this.myData ", this.myData)
  }
  send() {
    this.myform.value.email = this.main_userid

    if (this.myform.valid) {
      this.http.post(this.baseurl + "admin-panel/addOrupdate", this.myform.value).subscribe(res => {
        this.createuserRes = res
        console.log(this.createuserRes);
        if (this.createuserRes.status == true) {
          window.location.reload()
        }
      })

    }

    else {

      for (const control of Object.keys(this.myform.controls)) {
        this.myform.controls[control].markAsTouched();
      }
      return;

      // this.toastr.error("Les champs d'entrée sont requis", 'Error', {
      //   timeOut: 3000,
      // });
    }


  }
}
