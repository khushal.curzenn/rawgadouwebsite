import { Component, OnInit,HostListener } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { UserServiceService } from '../user-service.service';
@Component({
  selector: 'app-category-filter',
  templateUrl: './category-filter.component.html',
  styleUrls: ['./category-filter.component.css']
})
export class CategoryFilterComponent implements OnInit {
  all_Category:any[] = [];
  items: any;
  p: any;
  baseurl: any
  navURL: any; navURLRes: any; itemLength: number=0;
  myoptions: any
  categoryURL: any
  parentCat: any
  catId: any
  categoryURLres: any
  myrating:any; minval:any; maxval:any; condition:any
  query:string="";
  allproductsApi: any
  apiResponse:any;
  all_Category_new:any[] = [];
  starRating:any
  addFavProduct: any; addFavProductRes: any
  homepro: any
  user_id: any
  deleteFavProduct: any; deleteFavProductRes: any
  QWres:any
  singleQWProduct:any
  classForPriceRadio_1:string=""; classForPriceRadio_2:string=""; classForPriceRadio_3:string=""; classForPriceRadio_4:string="";
  classForPriceRadio_5:string=""; classForPriceRadio_6:string="";
  classForConditionRadio:string="";
  classForRatingRadio:string="";
  test:any;
  totalPageNumber:number=0; numbers:any;selectedIndex:number=0;
  apiResponseCheck:number = 1;
  sortVar:number=1;
  auctionVal:Boolean=false; front_url:string="";
  constructor(private http: HttpClient, private user: UserServiceService,
    private router: Router, private route: ActivatedRoute) {
      
    this.baseurl = user.baseapiurl2
    this.user_id = localStorage.getItem('main_userid')
    this.navURL = this.baseurl + "api/product/listProducts"
    this.allproductsApi = this.baseurl + "api/product/getallproducts";

    this.catId = this.route.snapshot.queryParams['cat_id'];
   // console.log("this.catId ",this.catId);
   
   
    this.front_url = user.front_url;


    this.categoryURL = this.baseurl + "api/product/getOnlyChildCategory/";
    this.addFavProduct = this.baseurl + "api/product/addFavProduct"
    this.deleteFavProduct = this.baseurl + "api/product/deleteFavProduct/"
    this.myoptions = {
      category_id: this.catId,
      pagno: 0,
      sort: 1,
      pricerangeLow: "",
      pricerangeHigh: "",
      rating: "",
      condition: ""

    }
    this.myrating = '';
    this.minval ='';
    this.maxval='';
    this.condition ='';
    this.starRating = 3.5
    
    this.getProducts(0);
    this.getProductsCount();
    this.getChildCategory()

    this.test = ''
  }

  ngOnInit(): void {
  }

  selectAllProduct(e:any)
  {
    console.log("hereee");
    window.location.href = this.front_url+"productList";
  }
  //
  getProductsCount() {
    console.log("get product  function called ---- >>>>> ");
     
    
    if(this.route.snapshot.queryParams['query'] != "" && this.route.snapshot.queryParams['query'] != null)
    {
      this.query = this.route.snapshot.queryParams['query'];
    }
    console.log("query ---- >>>>> ",this.query);
    
    const params = {
      category_id: this.catId,
      pagno: 0,
      sort: 1,
      pricerangeLow: this.minval,
      pricerangeHigh: this.maxval,
      rating: this.myrating,
      condition: this.condition,
      auctionVal: this.auctionVal,
      query: this.query,
    }
    console.log("called 92");
    console.log("this.catId ",this.catId);
    this.items = [];
    this.http.post(this.baseurl + "api/product/listProductsTotalCount", params).subscribe(res => {
      this.apiResponse = res;
      this.totalPageNumber = this.apiResponse.data;
      this.numbers = Array(this.totalPageNumber).fill(0).map((m,n)=>n);
      console.log("this.numbers ",this.numbers);
      // this.items = this.navURLRes.data;
      // console.log("this.items ", this.items );
      // if(this.items)
      // {
      //   //this.itemLength = this.items.length
      // }
    })

  }
  sortData(page_no:any,e:any)
  {

    let sorting_var = e.target.value;
    console.log("this.sorting_var ", sorting_var);
    console.log("page_no ", page_no);
    this.sortVar = sorting_var;
    this.getProducts(page_no)
  }
  getProducts(page_no:any) {
    this.selectedIndex = page_no;
    console.log("get product  function called ---- >>>>> ");
    // if(this.route.snapshot.queryParams['cat_id'] != "" && this.route.snapshot.queryParams['cat_id'] != null)
    // {
    //   this.catId = this.route.snapshot.queryParams['cat_id']
    // }

    // // if(this.route.snapshot.queryParams['myrating'] != "" && this.route.snapshot.queryParams['myrating'] != null)
    // // {
    // //   this.myrating = this.route.snapshot.queryParams['myrating'];
    // // }
    // if(this.route.snapshot.queryParams['myrating'] != "" && this.route.snapshot.queryParams['myrating'] != null)
    // {
    //   this.myrating = this.route.snapshot.queryParams['myrating'];
    // }
    // if(this.route.snapshot.queryParams['condition'] != "" && this.route.snapshot.queryParams['condition'] != null)
    // {
    //   this.condition = this.route.snapshot.queryParams['condition'];
    // }
    // if(this.route.snapshot.queryParams['minval'] != "" && this.route.snapshot.queryParams['minval'] != null)
    // {
    //   this.minval = this.route.snapshot.queryParams['minval'];
    // }
    // if(this.route.snapshot.queryParams['maxval'] != "" && this.route.snapshot.queryParams['maxval'] != null)
    // {
    //   this.maxval = this.route.snapshot.queryParams['maxval'];
    // }
    
    if(this.route.snapshot.queryParams['query'] != "" && this.route.snapshot.queryParams['query'] != null)
    {
      this.query = this.route.snapshot.queryParams['query'];
    }
    console.log("query ---- >>>>> ",this.query);
    
    const params = {
      category_id: this.catId,
      pagno: page_no,
      sort: this.sortVar,
      pricerangeLow: this.minval,
      pricerangeHigh: this.maxval,
      rating: this.myrating,
      condition: this.condition,
      auctionVal: this.auctionVal,
      query: this.query,
    }
    console.log("called 92");
    console.log("this.catId ",this.catId);
    this.items = [];
    this.apiResponseCheck = 1;
    this.http.post(this.navURL, params).subscribe(res => {
      this.navURLRes = res;

      this.items = this.navURLRes.data;
      this.apiResponseCheck = 0;
      console.log("this.items ", this.items );
      if(this.items)
      {
        this.itemLength = this.items.length
      }
    })

  }
  getChildCategory() {
    if(this.catId)
    {
      this.http.get(this.categoryURL + this.catId).subscribe(res => {
        this.categoryURLres = res
        this.parentCat = this.categoryURLres.data;
        if(this.categoryURLres.status == true)
        {
          //console.log("cat_record" , this.categoryURLres.cat_record);
          if(this.categoryURLres.cat_record)
          {
            let objj = {"id":this.categoryURLres.cat_record._id,"title":this.categoryURLres.cat_record.title}
            this.all_Category.push(objj);
            console.log("all_Category 126" , this.all_Category);
          }
        }
        //
        //console.log(this.parentCat, "sdygdy")
      })

      //this.baseurl + "api/product/getOnlyChildCategory/";
      // this.http.get(this.baseurl + "api/product/getAllParentCategory/" + this.catId).subscribe(res => {
      //   this.apiResponse = res
      //   console.log("All parent category ", this.apiResponse);

      //   //console.log(this.parentCat, "sdygdy")
      // })
    }
  }
  removeCategory(id:any)
  {
    this.all_Category_new = [];
    for(let x=0; x<this.all_Category.length; x++)
    {
      if(this.all_Category[x].id == id)
      {
        break;
      }
      this.all_Category_new.push(this.all_Category[x]);
    }
      
    
    //console.log("all_Category_new ",this.all_Category_new);
    this.all_Category = [];
    this.all_Category = this.all_Category_new;

    console.log("removed all_Category 158",this.all_Category);

    this.catId =   id;
    //console.log("this.catId ",this.catId);
    this.router.navigate(['.'], { relativeTo: this.route, queryParams: { 
      "cat_id":this.catId,
     }, queryParamsHandling: 'merge'});
    this.getChildCategory();
    this.getProducts(0);
    this.getProductsCount();
  }
  selectRating(event: any) {
    this.myrating = event.target.value

    //console.log("this.myrating ", this.myrating);

    this.router.navigate(['.'], { relativeTo: this.route, queryParams: { 
      "myrating":this.myrating,
     }, queryParamsHandling: 'merge'});
     this.getProducts(0);
     this.getProductsCount();
  }

  selectprice(event:any,i:number)
  {
    console.log("event  " , event);
    
    this.classForPriceRadio_1=""; this.classForPriceRadio_2=""; this.classForPriceRadio_3=""; this.classForPriceRadio_4="";
    this.classForPriceRadio_5=""; this.classForPriceRadio_6 ="";


    console.log("this.minval ", this.minval);
    console.log("this.maxval ", this.maxval);
    if(this.minval == event.target.id && this.maxval == event.target.value)
    {
      console.log("iffff 201");
      this.minval= ""
      this.maxval = ""

      console.log("this.minval ", this.minval);
      console.log("this.maxval ", this.maxval);

      this.router.navigate(['.'], { relativeTo: this.route, queryParams: { 
        "minval":this.minval,
        "maxval":this.maxval,
      }, queryParamsHandling: 'merge'});
    }else{
      if(i == 1){
        this.classForPriceRadio_1 = "classForPriceRadio_1";
      }else if(i == 2){
        this.classForPriceRadio_2 = "classForPriceRadio_1";
      }else if(i == 3){
        this.classForPriceRadio_3 = "classForPriceRadio_1";
      }else if(i == 4){
        this.classForPriceRadio_4 = "classForPriceRadio_1";
      }else if(i == 5){
        this.classForPriceRadio_5 = "classForPriceRadio_1";
      }else if(i == 6){
        this.classForPriceRadio_6 = "classForPriceRadio_1";
      }
      
      console.log("elseee 213");
      this.minval= event.target.id
      this.maxval = event.target.value

      console.log("this.minval ", this.minval);
      console.log("this.maxval ", this.maxval);

      this.router.navigate(['.'], { relativeTo: this.route, queryParams: { 
        "minval":this.minval,
        "maxval":this.maxval,
      }, queryParamsHandling: 'merge'});
    }
    
     this.getProducts(0);
     this.getProductsCount();

  }

  selectcondition(event: any) {
    console.log("old value  ", this.condition);
    if(this.condition == event.target.value)
    {
      this.condition = "";
      console.log("new value ",this.condition);
      
      this.router.navigate(['.'], { relativeTo: this.route, queryParams: { 
        "condition":"",
      }, queryParamsHandling: 'merge'});

    }else{
      this.condition = event.target.value;
      console.log("new value ",this.condition);
      
      this.router.navigate(['.'], { relativeTo: this.route, queryParams: { 
        "condition":this.condition,
      }, queryParamsHandling: 'merge'});

    }
    
    // const params = {
    //   category_id: this.catId,
    //   pagno: 0,
    //   sort: 1,
    //   pricerangeLow: "",
    //   pricerangeHigh: "",
    //   rating: "",
    //   condition: this.myrating

    // }

    // this.http.post( this.allproductsApi,params).subscribe(res=>{
    //   this.navURLRes = res
    //   // console.log(this.navURLRes)
    //   this.items =  this.navURLRes.data.products
    //   this.itemLength = this.items.length
     
    // })
     this.getProducts(0);
     this.getProductsCount();

  }
  isAuctionClick(e:any)
  {
    this.auctionVal = e.target.checked;
    console.log("val ",e.target.checked);
    this.router.navigate(['.'], { relativeTo: this.route, queryParams: { 
      "isAuction":this.auctionVal,
     }, queryParamsHandling: 'merge'});
     this.getProducts(0);
     this.getProductsCount();
  }
  clickCategory(id: any) {
    this.catId =   id;
    //console.log("this.catId ",this.catId);
    this.router.navigate(['.'], { relativeTo: this.route, queryParams: { 
      "cat_id":this.catId,
     }, queryParamsHandling: 'merge'});
    console.log("called 170");
     this.getChildCategory();
     this.getProducts(0);
     this.getProductsCount();
  }
  addFavItem(productid: any) {

    const parms = {
      "product_id": productid,
      "user_id": this.user_id
    }

    this.http.post(this.addFavProduct, parms).subscribe(res => {
      this.addFavProductRes = res

      if (this.addFavProductRes.status) {
        window.location.reload()
      }
    })

  }
  removeFavItem(productid: any) {

    this.http.get(this.deleteFavProduct + productid).subscribe(res => {
      this.deleteFavProductRes = res
      console.log(res)

      if (this.deleteFavProductRes.status) {
        window.location.reload()
      }
    })
  }

  openQW(Pid:any){
    this.test = Pid 
    console.log(Pid)
    const params = {
      "singleid": Pid,
    }
    
    this.http.post( this.allproductsApi,params).subscribe(res=>{
      
      this.QWres = res
      if(this.QWres.status){
        this.singleQWProduct = this.QWres.data[0]

        console.log(this.singleQWProduct)
      }
     
    })

  }


  @HostListener('window:scroll', [])
  onScroll(): void 
  {
    console.log("in bottom");
    
  }

}
