import { HttpClient } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UserServiceService } from '../user-service.service';
import { Router } from '@angular/router';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-edit-address',
  templateUrl: './edit-address.component.html',
  styleUrls: ['./edit-address.component.css']
})
export class EditAddressComponent implements OnInit {
  CheckoutProducts: any; addressform!: FormGroup
  myAddressForm: any; user_email: any; baseurl: any; addressURL: any
  fulldata: any; getAddressURL: any; user_id: any
  myaddressArray: any; Accesscode: any; Intephone: any; instructions: any
  latitude:number=0;
  longitude:number=0;
  endAddress: any;apiResponse:any;api_response:any;
  myCountryCode:any
  my_country: any; my_city: any; my_po:any; countries:any;
  nrSelect = "+221";
  eee:any;   googleAddress:any;
  old_rec :any;
  old_name :any; old_country :any; old_countryCode :any; old_telephone :any; old_address :any;  
      addressss:any;
        old_pobox:any; old_city:any; old_accesscode:any;    old_interphone:any;    old_instructions:any;address_idd:any;
  constructor(private user: UserServiceService, private http: HttpClient,
    private router: Router,@Inject(MAT_DIALOG_DATA) public data: any) {

      this.Accesscode = ''
      this.Intephone = ''
      this.instructions = '';

    this.baseurl = user.baseapiurl2
    this.user_email = localStorage.getItem("useremail")
    this.user_id = localStorage.getItem("main_userid")
    this.addressURL = this.baseurl + "api/user/updateAddressDetail"
    this.getAddressURL = this.baseurl + "api/user/getuserprofile/"
   
    this.CheckoutProducts = []
    this.myaddressArray = []
    this.my_country = ''
    this.my_city = ''
    this.my_po = ''
    this.myAddressForm = {
      email: this.user_email
    }

    this.http.post(this.baseurl + "api/user/getUserSingleAddress/", {id:this.data.address_id,user_id:this.user_id}).subscribe(res => {
       
      this.api_response = res; 
      console.log("this.apiResponse  ",this.api_response )
      this.old_rec = this.api_response.data;
      console.log("this.old_rec ", this.old_rec);
      console.log("this.old_rec ", this.old_rec.name);
      this.old_name = this.old_rec.name;
      this.my_country = this.old_rec.country;
      this.old_countryCode = this.old_rec.countryCode;
      this.old_telephone = this.old_rec.telephone;
      this.old_address = this.old_rec.address.address;
      this.googleAddress = this.old_rec.address.address;
      this.addressss = this.old_rec.address;
      this.my_po = this.old_rec.pobox;
      this.my_city = this.old_rec.city;
      //this.old_city = this.old_rec.address.city;

      console.log("this.old_rec.delivery_instructions ", this.old_rec.delivery_instructions);
      
      if(this.old_rec.delivery_instructions)
      {
        console.log("hereeeee 69");
        this.old_accesscode = this.old_rec.delivery_instructions.accesscode;
        this.old_interphone = this.old_rec.delivery_instructions.interphone;
        this.old_instructions = this.old_rec.delivery_instructions.instructions;
        
      }
     
     


      if(this.old_accesscode == undefined || this.old_accesscode == 'undefined')
      {
        console.log("this.old_accesscode ", this.old_accesscode)
        this.old_accesscode = "";
      }

      if(this.old_interphone == undefined || this.old_interphone == 'undefined')
      {
        this.old_interphone = "";
      }

      if(this.old_instructions == undefined || this.old_instructions == 'undefined')
      {
        this.old_instructions = "";
      }


      if(this.old_rec.address)
      {
        if(this.old_rec.address.latlong)
        {
          if(this.old_rec.address.latlong.length > 1)
          {
            this.longitude = this.old_rec.address.latlong[0];
            this.latitude = this.old_rec.address.latlong[1];
          }
        }
      }
    

    })


    this.addressform = new FormGroup({
      name: new FormControl('', Validators.required),
      telephone: new FormControl('', Validators.required),
      address: new FormControl('', Validators.required),
      po_box: new FormControl('' ),
      country: new FormControl(''),
      city: new FormControl(''),
      latitude: new FormControl(''),
      longitude: new FormControl(''),
      countryCode: new FormControl(''),
      address_id: new FormControl(''),
      access_code: new FormControl(''),
      interphone: new FormControl(''),
      instruction: new FormControl(''),
    })
   


    this.countries = [
      {
        "code": "+7 840",
        "name": "Abkhazia"
      },
      {
        "code": "+93",
        "name": "Afghanistan"
      },
      {
        "code": "+355",
        "name": "Albania"
      },
      {
        "code": "+213",
        "name": "Algeria"
      },
      {
        "code": "+1 684",
        "name": "American Samoa"
      },
      {
        "code": "+376",
        "name": "Andorra"
      },
      {
        "code": "+244",
        "name": "Angola"
      },
      {
        "code": "+1 264",
        "name": "Anguilla"
      },
      {
        "code": "+1 268",
        "name": "Antigua and Barbuda"
      },
      {
        "code": "+54",
        "name": "Argentina"
      },
      {
        "code": "+374",
        "name": "Armenia"
      },
      {
        "code": "+297",
        "name": "Aruba"
      },
      {
        "code": "+247",
        "name": "Ascension"
      },
      {
        "code": "+61",
        "name": "Australia"
      },
      {
        "code": "+672",
        "name": "Australian External Territories"
      },
      {
        "code": "+43",
        "name": "Austria"
      },
      {
        "code": "+994",
        "name": "Azerbaijan"
      },
      {
        "code": "+1 242",
        "name": "Bahamas"
      },
      {
        "code": "+973",
        "name": "Bahrain"
      },
      {
        "code": "+880",
        "name": "Bangladesh"
      },
      {
        "code": "+1 246",
        "name": "Barbados"
      },
      {
        "code": "+1 268",
        "name": "Barbuda"
      },
      {
        "code": "+375",
        "name": "Belarus"
      },
      {
        "code": "+32",
        "name": "Belgium"
      },
      {
        "code": "+501",
        "name": "Belize"
      },
      {
        "code": "+229",
        "name": "Benin"
      },
      {
        "code": "+1 441",
        "name": "Bermuda"
      },
      {
        "code": "+975",
        "name": "Bhutan"
      },
      {
        "code": "+591",
        "name": "Bolivia"
      },
      {
        "code": "+387",
        "name": "Bosnia and Herzegovina"
      },
      {
        "code": "+267",
        "name": "Botswana"
      },
      {
        "code": "+55",
        "name": "Brazil"
      },
      {
        "code": "+246",
        "name": "British Indian Ocean Territory"
      },
      {
        "code": "+1 284",
        "name": "British Virgin Islands"
      },
      {
        "code": "+673",
        "name": "Brunei"
      },
      {
        "code": "+359",
        "name": "Bulgaria"
      },
      {
        "code": "+226",
        "name": "Burkina Faso"
      },
      {
        "code": "+257",
        "name": "Burundi"
      },
      {
        "code": "+855",
        "name": "Cambodia"
      },
      {
        "code": "+237",
        "name": "Cameroon"
      },
      {
        "code": "+1",
        "name": "Canada"
      },
      {
        "code": "+238",
        "name": "Cape Verde"
      },
      {
        "code": "+ 345",
        "name": "Cayman Islands"
      },
      {
        "code": "+236",
        "name": "Central African Republic"
      },
      {
        "code": "+235",
        "name": "Chad"
      },
      {
        "code": "+56",
        "name": "Chile"
      },
      {
        "code": "+86",
        "name": "China"
      },
      {
        "code": "+61",
        "name": "Christmas Island"
      },
      {
        "code": "+61",
        "name": "Cocos-Keeling Islands"
      },
      {
        "code": "+57",
        "name": "Colombia"
      },
      {
        "code": "+269",
        "name": "Comoros"
      },
      {
        "code": "+242",
        "name": "Congo"
      },
      {
        "code": "+243",
        "name": "Congo, Dem. Rep. of (Zaire)"
      },
      {
        "code": "+682",
        "name": "Cook Islands"
      },
      {
        "code": "+506",
        "name": "Costa Rica"
      },
      {
        "code": "+385",
        "name": "Croatia"
      },
      {
        "code": "+53",
        "name": "Cuba"
      },
      {
        "code": "+599",
        "name": "Curacao"
      },
      {
        "code": "+537",
        "name": "Cyprus"
      },
      {
        "code": "+420",
        "name": "Czech Republic"
      },
      {
        "code": "+45",
        "name": "Denmark"
      },
      {
        "code": "+246",
        "name": "Diego Garcia"
      },
      {
        "code": "+253",
        "name": "Djibouti"
      },
      {
        "code": "+1 767",
        "name": "Dominica"
      },
      {
        "code": "+1 809",
        "name": "Dominican Republic"
      },
      {
        "code": "+670",
        "name": "East Timor"
      },
      {
        "code": "+56",
        "name": "Easter Island"
      },
      {
        "code": "+593",
        "name": "Ecuador"
      },
      {
        "code": "+20",
        "name": "Egypt"
      },
      {
        "code": "+503",
        "name": "El Salvador"
      },
      {
        "code": "+240",
        "name": "Equatorial Guinea"
      },
      {
        "code": "+291",
        "name": "Eritrea"
      },
      {
        "code": "+372",
        "name": "Estonia"
      },
      {
        "code": "+251",
        "name": "Ethiopia"
      },
      {
        "code": "+500",
        "name": "Falkland Islands"
      },
      {
        "code": "+298",
        "name": "Faroe Islands"
      },
      {
        "code": "+679",
        "name": "Fiji"
      },
      {
        "code": "+358",
        "name": "Finland"
      },
      {
        "code": "+33",
        "name": "France"
      },
      {
        "code": "+596",
        "name": "French Antilles"
      },
      {
        "code": "+594",
        "name": "French Guiana"
      },
      {
        "code": "+689",
        "name": "French Polynesia"
      },
      {
        "code": "+241",
        "name": "Gabon"
      },
      {
        "code": "+220",
        "name": "Gambia"
      },
      {
        "code": "+995",
        "name": "Georgia"
      },
      {
        "code": "+49",
        "name": "Germany"
      },
      {
        "code": "+233",
        "name": "Ghana"
      },
      {
        "code": "+350",
        "name": "Gibraltar"
      },
      {
        "code": "+30",
        "name": "Greece"
      },
      {
        "code": "+299",
        "name": "Greenland"
      },
      {
        "code": "+1 473",
        "name": "Grenada"
      },
      {
        "code": "+590",
        "name": "Guadeloupe"
      },
      {
        "code": "+1 671",
        "name": "Guam"
      },
      {
        "code": "+502",
        "name": "Guatemala"
      },
      {
        "code": "+224",
        "name": "Guinea"
      },
      {
        "code": "+245",
        "name": "Guinea-Bissau"
      },
      {
        "code": "+595",
        "name": "Guyana"
      },
      {
        "code": "+509",
        "name": "Haiti"
      },
      {
        "code": "+504",
        "name": "Honduras"
      },
      {
        "code": "+852",
        "name": "Hong Kong SAR China"
      },
      {
        "code": "+36",
        "name": "Hungary"
      },
      {
        "code": "+354",
        "name": "Iceland"
      },
      {
        "code": "+91",
        "name": "India"
      },
      {
        "code": "+62",
        "name": "Indonesia"
      },
      {
        "code": "+98",
        "name": "Iran"
      },
      {
        "code": "+964",
        "name": "Iraq"
      },
      {
        "code": "+353",
        "name": "Ireland"
      },
      {
        "code": "+972",
        "name": "Israel"
      },
      {
        "code": "+39",
        "name": "Italy"
      },
      {
        "code": "+1 876",
        "name": "Jamaica"
      },
      {
        "code": "+81",
        "name": "Japan"
      },
      {
        "code": "+962",
        "name": "Jordan"
      },
      {
        "code": "+7 7",
        "name": "Kazakhstan"
      },
      {
        "code": "+254",
        "name": "Kenya"
      },
      {
        "code": "+686",
        "name": "Kiribati"
      },
      {
        "code": "+965",
        "name": "Kuwait"
      },
      {
        "code": "+996",
        "name": "Kyrgyzstan"
      },
      {
        "code": "+856",
        "name": "Laos"
      },
      {
        "code": "+371",
        "name": "Latvia"
      },
      {
        "code": "+961",
        "name": "Lebanon"
      },
      {
        "code": "+266",
        "name": "Lesotho"
      },
      {
        "code": "+231",
        "name": "Liberia"
      },
      {
        "code": "+218",
        "name": "Libya"
      },
      {
        "code": "+423",
        "name": "Liechtenstein"
      },
      {
        "code": "+370",
        "name": "Lithuania"
      },
      {
        "code": "+352",
        "name": "Luxembourg"
      },
      {
        "code": "+853",
        "name": "Macau SAR China"
      },
      {
        "code": "+389",
        "name": "Macedonia"
      },
      {
        "code": "+261",
        "name": "Madagascar"
      },
      {
        "code": "+265",
        "name": "Malawi"
      },
      {
        "code": "+60",
        "name": "Malaysia"
      },
      {
        "code": "+960",
        "name": "Maldives"
      },
      {
        "code": "+223",
        "name": "Mali"
      },
      {
        "code": "+356",
        "name": "Malta"
      },
      {
        "code": "+692",
        "name": "Marshall Islands"
      },
      {
        "code": "+596",
        "name": "Martinique"
      },
      {
        "code": "+222",
        "name": "Mauritania"
      },
      {
        "code": "+230",
        "name": "Mauritius"
      },
      {
        "code": "+262",
        "name": "Mayotte"
      },
      {
        "code": "+52",
        "name": "Mexico"
      },
      {
        "code": "+691",
        "name": "Micronesia"
      },
      {
        "code": "+1 808",
        "name": "Midway Island"
      },
      {
        "code": "+373",
        "name": "Moldova"
      },
      {
        "code": "+377",
        "name": "Monaco"
      },
      {
        "code": "+976",
        "name": "Mongolia"
      },
      {
        "code": "+382",
        "name": "Montenegro"
      },
      {
        "code": "+1664",
        "name": "Montserrat"
      },
      {
        "code": "+212",
        "name": "Morocco"
      },
      {
        "code": "+95",
        "name": "Myanmar"
      },
      {
        "code": "+264",
        "name": "Namibia"
      },
      {
        "code": "+674",
        "name": "Nauru"
      },
      {
        "code": "+977",
        "name": "Nepal"
      },
      {
        "code": "+31",
        "name": "Netherlands"
      },
      {
        "code": "+599",
        "name": "Netherlands Antilles"
      },
      {
        "code": "+1 869",
        "name": "Nevis"
      },
      {
        "code": "+687",
        "name": "New Caledonia"
      },
      {
        "code": "+64",
        "name": "New Zealand"
      },
      {
        "code": "+505",
        "name": "Nicaragua"
      },
      {
        "code": "+227",
        "name": "Niger"
      },
      {
        "code": "+234",
        "name": "Nigeria"
      },
      {
        "code": "+683",
        "name": "Niue"
      },
      {
        "code": "+672",
        "name": "Norfolk Island"
      },
      {
        "code": "+850",
        "name": "North Korea"
      },
      {
        "code": "+1 670",
        "name": "Northern Mariana Islands"
      },
      {
        "code": "+47",
        "name": "Norway"
      },
      {
        "code": "+968",
        "name": "Oman"
      },
      {
        "code": "+92",
        "name": "Pakistan"
      },
      {
        "code": "+680",
        "name": "Palau"
      },
      {
        "code": "+970",
        "name": "Palestinian Territory"
      },
      {
        "code": "+507",
        "name": "Panama"
      },
      {
        "code": "+675",
        "name": "Papua New Guinea"
      },
      {
        "code": "+595",
        "name": "Paraguay"
      },
      {
        "code": "+51",
        "name": "Peru"
      },
      {
        "code": "+63",
        "name": "Philippines"
      },
      {
        "code": "+48",
        "name": "Poland"
      },
      {
        "code": "+351",
        "name": "Portugal"
      },
      {
        "code": "+1 787",
        "name": "Puerto Rico"
      },
      {
        "code": "+974",
        "name": "Qatar"
      },
      {
        "code": "+262",
        "name": "Reunion"
      },
      {
        "code": "+40",
        "name": "Romania"
      },
      {
        "code": "+7",
        "name": "Russia"
      },
      {
        "code": "+250",
        "name": "Rwanda"
      },
      {
        "code": "+685",
        "name": "Samoa"
      },
      {
        "code": "+378",
        "name": "San Marino"
      },
      {
        "code": "+966",
        "name": "Saudi Arabia"
      },
      {
        "code": "+381",
        "name": "Serbia"
      },
      {
        "code": "+248",
        "name": "Seychelles"
      },
      {
        "code": "+232",
        "name": "Sierra Leone"
      },
      {
        "code": "+65",
        "name": "Singapore"
      },
      {
        "code": "+421",
        "name": "Slovakia"
      },
      {
        "code": "+386",
        "name": "Slovenia"
      },
      {
        "code": "+677",
        "name": "Solomon Islands"
      },
      {
        "code": "+27",
        "name": "South Africa"
      },
      {
        "code": "+500",
        "name": "South Georgia and the South Sandwich Islands"
      },
      {
        "code": "+82",
        "name": "South Korea"
      },
      {
        "code": "+34",
        "name": "Spain"
      },
      {
        "code": "+94",
        "name": "Sri Lanka"
      },
      {
        "code": "+249",
        "name": "Sudan"
      },
      {
        "code": "+597",
        "name": "Suriname"
      },
      {
        "code": "+268",
        "name": "Swaziland"
      },
      {
        "code": "+46",
        "name": "Sweden"
      },
      {
        "code": "+41",
        "name": "Switzerland"
      },
      {
        "code": "+963",
        "name": "Syria"
      },
      {
        "code": "+886",
        "name": "Taiwan"
      },
      {
        "code": "+992",
        "name": "Tajikistan"
      },
      {
        "code": "+255",
        "name": "Tanzania"
      },
      {
        "code": "+66",
        "name": "Thailand"
      },
      {
        "code": "+670",
        "name": "Timor Leste"
      },
      {
        "code": "+228",
        "name": "Togo"
      },
      {
        "code": "+690",
        "name": "Tokelau"
      },
      {
        "code": "+676",
        "name": "Tonga"
      },
      {
        "code": "+1 868",
        "name": "Trinidad and Tobago"
      },
      {
        "code": "+216",
        "name": "Tunisia"
      },
      {
        "code": "+90",
        "name": "Turkey"
      },
      {
        "code": "+993",
        "name": "Turkmenistan"
      },
      {
        "code": "+1 649",
        "name": "Turks and Caicos Islands"
      },
      {
        "code": "+688",
        "name": "Tuvalu"
      },
      {
        "code": "+1 340",
        "name": "U.S. Virgin Islands"
      },
      {
        "code": "+256",
        "name": "Uganda"
      },
      {
        "code": "+380",
        "name": "Ukraine"
      },
      {
        "code": "+971",
        "name": "United Arab Emirates"
      },
      {
        "code": "+44",
        "name": "United Kingdom"
      },
      {
        "code": "+1",
        "name": "United States"
      },
      {
        "code": "+598",
        "name": "Uruguay"
      },
      {
        "code": "+998",
        "name": "Uzbekistan"
      },
      {
        "code": "+678",
        "name": "Vanuatu"
      },
      {
        "code": "+58",
        "name": "Venezuela"
      },
      {
        "code": "+84",
        "name": "Vietnam"
      },
      {
        "code": "+1 808",
        "name": "Wake Island"
      },
      {
        "code": "+681",
        "name": "Wallis and Futuna"
      },
      {
        "code": "+967",
        "name": "Yemen"
      },
      {
        "code": "+260",
        "name": "Zambia"
      },
      {
        "code": "+255",
        "name": "Zanzibar"
      },
      {
        "code": "+263",
        "name": "Zimbabwe"
      },
      {
        "code": "+225",
        "name": "Ivory Coast"
      },
      {
        "code": "+221",
        "name": "Senegal"
      }
    ]
    this.myCountryCode = '+221';

     //
     
  }

  ngOnInit(): void {
    console.log(this.data);
    this.address_idd = this.data.address_id;
  }

  editAddress() {
    console.log("adderss submit 54");
    const delivery_instructions = {
      accesscode: this.addressform.value.access_code,
      interphone: this.addressform.value.interphone,
      requiredcode: '',
      instructions: this.addressform.value.instruction,
    }
    this.addressform.value.delivery_instructions = delivery_instructions


    let addressss =  {
      address: this.googleAddress,
      latlong: [ this.longitude,this.latitude ]
    }


    this.myAddressForm.address = this.addressform.value
    this.myAddressForm.addresss = addressss
    this.myAddressForm.address.address = this.addressform.value.address;
    this.myAddressForm.address.city = this.addressform.value.city
    this.myAddressForm.address.country = this.addressform.value.country
    //this.myAddressForm.address.pobox = this.my_po;
     
    this.myAddressForm.address.pobox = this.addressform.value.po_box;
    this.myAddressForm.address_id = this.address_idd;
     
    console.log("this.addressform.value.pobox ", this.addressform.value.po_box);
    console.log("this.myAddressForm.address.pobox ", this.myAddressForm.address.pobox );
    
    //this.longitude = parseFloat(this.longitude); 

    this.myAddressForm.address.Location = {
      type:"Point",
      coordinates:[this.longitude,this.latitude]
    }
    //endLocation: { type: 'Point', coordinates: [ 48.8676635, 2.3640304 ] }
    // this.myAddressForm.address.latitude = this.latitude;
    // this.myAddressForm.address.longitude = this.longitude;

    console.log("myAddressForm ",this.myAddressForm)
    if (this.addressform.valid)
    {
      console.log("form valid 71");
      // if(this.latitude == 0 || this.latitude == null || this.latitude == undefined || this.longitude == 0 || this.longitude == null || this.longitude == undefined)
      // {
      //   this.apiResponse = {status:false,errorMessage:"L'adresse ne doit être que l'adresse de Google"};
      //   return;
      // }else{}
        this.http.post(this.addressURL, this.myAddressForm).subscribe(res => {
          this.fulldata = res;
          this.apiResponse = res;
          console.log(this.fulldata)
          console.log("this.apiResponse  ",this.apiResponse )
          if (this.fulldata.status == true) {
            //window.location.reload()
            setTimeout(()=>{
              window.location.reload()
            },2000);
            
          }
        })
      
      //return false;
      
    }
    else{
      console.log("form in valid 84");
      for (const control of Object.keys(this.addressform.controls)) {
        this.addressform.controls[control].markAsTouched();
      }
      return;
    }




  }

  handleAddressChange2(address2: any) {
    console.log(address2.address_components);
    console.log("address2 " , address2.formatted_address);
    console.log("address2 " , address2);
    this.googleAddress = address2.formatted_address;


    this.latitude = address2.geometry.location.lat()
    this.longitude = address2.geometry.location.lng()

    var country = address2.address_components.filter((component: { types: string | string[]; }) => component.types.includes("country"))
     var po_box = address2.address_components.filter((component: { types: string | string[]; }) => component.types.includes("postal_code"))
    var city = address2.address_components.filter((component: { types: string | string[]; }) => component.types.includes("locality"))
    ///console.log(country[0].long_name, city[0].long_name)
    if(country)
    {
      if(country.length > 0)
      { 
        this.my_country = country[0].long_name;
        
        
      }
      if(city.length > 0)
      {
        this.my_city = city[0].long_name;
      }
      // if(po_box.length > 0)
      // {
      //   this.my_po = po_box[0].long_name;
      // }
    }
   

    this.endAddress = address2.formatted_address
    // this.endLatitude = address2.geometry.location.lat()
    // this.endLongitude = address2.geometry.location.lng()
    // this.mypickaddress.address = this.endAddress
    // this.mypickaddress.latlong = [this.endLatitude, this.endLongitude]


  }

  selectCounrtyCode(eve:any){
   
    this.myCountryCode = eve.target.value
  }
}
