import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-selleraddproduct',
  templateUrl: './selleraddproduct.component.html',
  styleUrls: ['./selleraddproduct.component.css']
})
export class SelleraddproductComponent implements OnInit {
  userForm!: FormGroup;
  phone:any;
  public products = [
    {
      id: 1,
      imgUrl: "",
      imgBase64Data: ""
    },
    {
      id: 2,
      imgUrl: "",
      imgBase64Data: ""
    },
    {
      id: 3,
      imgUrl: "",
      imgBase64Data: ""
    },
    {
      id: 4,
      imgUrl: "",
      imgBase64Data: ""
    },
    {
      id: 5,
      imgUrl: "",
      imgBase64Data: ""
    },
    {
      id: 6,
      imgUrl: "",
      imgBase64Data: ""
    },
    {
      id: 7,
      imgUrl: "",
      imgBase64Data: ""
    },
    {
      id: 8,
      imgUrl: "",
      imgBase64Data: ""
    },
    {
      id: 9,
      imgUrl: "",
      imgBase64Data: ""
    }
  ];
  url:any
  format:any

  constructor(
    private fb: FormBuilder
  ) {
    this.userForm = this.fb.group({
      name: [],
      phones: this.fb.array([
        this.fb.control(null)
      ]),
      phones2: this.fb.array([
        this.fb.control(null)
      ])
    })
  }

  ngOnInit(): void {
   
  }
  onFileUpdate(event:any, index: any) {
    console.log(index)
    const files = event.target.files;
    if (files.length === 0) return;

    const reader = new FileReader();

    reader.readAsDataURL(files[0]);
    reader.onload = _event => {
     
      this.products[index].imgBase64Data = reader.result as string;
    };
  }

  
  onSelectFile(event:any) {
    const file = event.target.files && event.target.files[0];
    if (file) {
      var reader = new FileReader();
      reader.readAsDataURL(file);
      if(file.type.indexOf('image')> -1){
        this.format = 'image';
      } else if(file.type.indexOf('video')> -1){
        this.format = 'video';
      }
      reader.onload = (event) => {
        this.url = (<FileReader>event.target).result;
      }
    }
  }

  addPhone(): void {
    (this.userForm.get('phones') as FormArray).push(
      this.fb.control(null)
    );
  }

  addPhone2(): void {
    (this.userForm.get('phones2') as FormArray).push(
      this.fb.control(null)
    );
  }

  removePhone(index:any) {
    (this.userForm.get('phones') as FormArray).removeAt(index);
  }

  removePhone2(index:any) {
    (this.userForm.get('phones2') as FormArray).removeAt(index);
  }


  getPhonesFormControls(): AbstractControl[] {
    return (<FormArray> this.userForm.get('phones')).controls
  }

  getPhonesFormControls2(): AbstractControl[] {
    return (<FormArray> this.userForm.get('phones2')).controls
  }

  send(values:any) {
    console.log(values);
  }

  
}
