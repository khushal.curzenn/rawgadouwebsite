import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdmindashboardComponent } from './admindashboard/admindashboard.component';
import { AdminpanelComponent } from './adminpanel/adminpanel.component';
import { CartComponent } from './cart/cart.component';
import { CategoryComponent } from './category/category.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { EvenementsComponent } from './evenements/evenements.component';
import { EventDetailComponent } from './event-detail/event-detail.component';
import { FilterProductsComponent } from './filter-products/filter-products.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { JeuxVideoEtConsoiesComponent } from './jeux-video-et-consoies/jeux-video-et-consoies.component';
import { LivresComponent } from './livres/livres.component';
import { LoginComponent } from './login/login.component';
import { MusiqueEtDvdComponent } from './musique-et-dvd/musique-et-dvd.component';
import { MyAccountComponent } from './my-account/my-account.component';
import { MyExpenditionComponent } from './my-expendition/my-expendition.component';
import { MyordersComponent } from './myorders/myorders.component';
import { MyproductsComponent } from './myproducts/myproducts.component';
import { NavbarComponent } from './navbar/navbar.component';
import { OrdershipComponent } from './ordership/ordership.component';
import { OtpComponent } from './otp/otp.component';
import { ProductOrderComponent } from './product-order/product-order.component';
import { RegisterComponent } from './register/register.component';
import { SelleraddproductComponent } from './selleraddproduct/selleraddproduct.component';
import { SellerdashboardComponent } from './sellerdashboard/sellerdashboard.component';
import { SellerhomeComponent } from './sellerhome/sellerhome.component';
import { SellerregisterComponent } from './sellerregister/sellerregister.component';
import { ShipmentComponent } from './shipment/shipment.component';
import { SigninComponent } from './signin/signin.component';
import { TablettesComponent } from './tablettes/tablettes.component';
import { TestComponent } from './test/test.component';

import { ViewProductComponent } from './view-product/view-product.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { MyaddressComponent } from './myaddress/myaddress.component';
import { SecurityComponent } from './security/security.component';
import { PromocodeComponent } from './promocode/promocode.component';
import { EventFavComponent } from './event-fav/event-fav.component';
import { CategoryFilterComponent } from './category-filter/category-filter.component';
import { OrderConfirmationComponent } from './order-confirmation/order-confirmation.component';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { MessegesChatComponent } from './messeges-chat/messeges-chat.component';
import { TarackingComponent } from './taracking/taracking.component';
import { EventBootcampComponent } from './event-bootcamp/event-bootcamp.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { TermsConditionsComponent } from './terms-conditions/terms-conditions.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { SellerProfileComponent } from './seller-profile/seller-profile.component';
import { WishListComponent } from './wish-list/wish-list.component';
import { ReturnOrderComponent } from './return-order/return-order.component';
import { RateSellerComponent } from './rate-seller/rate-seller.component';
import { ProductReviewComponent } from './product-review/product-review.component';
import { CancelOrderComponent } from './cancel-order/cancel-order.component';
import { ViewOrderComponent } from './view-order/view-order.component';
import { EventHomeComponent } from './event-home/event-home.component';
import { EventTicketsComponent } from './event-tickets/event-tickets.component';
import { EventlistComponent } from './events/eventlist/eventlist.component';
import { EventLikeComponent } from './event-like/event-like.component';
import { EventCartComponent } from './event-cart/event-cart.component';
import { EventConfirmComponent } from './event-confirm/event-confirm.component';
import { ChatInitiateComponent } from './chat-initiate/chat-initiate.component';
import { AbuseReportComponent } from './abuse-report/abuse-report.component';
import { PurchaseAuctionComponent } from './purchase-auction/purchase-auction.component';
import { MyBidComponent } from './my-bid/my-bid.component';
import { WonBidComponent } from './won-bid/won-bid.component';
import { OrderConfirmComboComponent } from './order-confirm-combo/order-confirm-combo.component';
import { AuctionCheckoutComponent } from './auction-checkout/auction-checkout.component';

import { BidCheckoutComponent } from './bid-checkout/bid-checkout.component';
import { CancelPaymentComponent } from './cancel-payment/cancel-payment.component';


const routes: Routes = [
  { path: 'navbar', component: NavbarComponent },
  { path: 'about_us', component: AboutUsComponent },
  { path: 'contact_us', component: ContactUsComponent },
  { path: 'term_conditions', component: TermsConditionsComponent },
  { path: 'your_personal', component: PrivacyPolicyComponent },
  { path: 'single_product', component: LivresComponent },
  { path: 'MusiqueEtDvd', component: MusiqueEtDvdComponent },
  {
    path: 's_dashboard',
    component: SellerdashboardComponent,
    children: [
      { path: '', component: MyExpenditionComponent, outlet: 'seller' },
      { path: 'shipment', component: ShipmentComponent, outlet: 'seller' },
      { path: 'ordertoship', component: OrdershipComponent, outlet: 'seller' },
      { path: 'myproducts', component: MyproductsComponent, outlet: 'seller' },
      { path: 'seller_addProduct', component: SelleraddproductComponent, outlet: 'seller' },
      { path: 'myorders', component: MyordersComponent, outlet: 'seller' },
    ]
  },
  {path: 'won_bid', component: WonBidComponent },
  {path: 'my_bid', component: MyBidComponent },
  {path: 'purchase_auction', component: PurchaseAuctionComponent},
  {path: 'myexpendition', component: MyExpenditionComponent},
  {path: 'seller_addProduct', component: SelleraddproductComponent},
  { path: 'JeuxVideoEtConsoies', component: JeuxVideoEtConsoiesComponent },
  { path: 'tablettes', component: TablettesComponent },
  { path: 'evenements', component: EvenementsComponent },
  { path: 'footer', component: FooterComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'signin', component: SigninComponent },
  { path: 'my_account', component: MyAccountComponent },
  { path: 'cart', component: CartComponent },
  { path: 'category', component: CategoryComponent},
  { path: 'productList', component: CategoryFilterComponent},
  { path: 'realhome', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'checkout', component: CheckoutComponent },
  { path: 'bidCheckout/:user_id/:product_id', component: BidCheckoutComponent },
  
  { path: 's_dashboard', component: SellerdashboardComponent },
  { path: 'seller_register', component: SellerregisterComponent },
  { path: 'seller_dashboard', component: SellerhomeComponent },
  { path: 'product_order', component: ProductOrderComponent },
  { path: 'event_favourite', component: EventFavComponent },
  { path: 'event_order_tickets', component: EventTicketsComponent },
  { path: 'event_like', component: EventLikeComponent },
  { path: 'my_events', component: MyExpenditionComponent },
  { path: 'my_address', component: MyaddressComponent },
  { path: 'promocode', component: PromocodeComponent },
  { path: 'connection_security', component: SecurityComponent },
  { path: 'view-product/:id', component: ViewProductComponent },
  { path: 'event-detail/:id', component: EventDetailComponent },
  { path: 'admin', component: AdminpanelComponent },
  { path: 'admin_dashboard', component: AdmindashboardComponent },
  { path: 'otp/:id', component: OtpComponent },
  { path: 'forget_password', component: ForgetPasswordComponent },
  { path: 'filter/:id', component: FilterProductsComponent },
  { path: 'userResetPassword/:id', component: ResetPasswordComponent },
  { path: 'order_confirm/:id', component: OrderConfirmationComponent },
  { path: 'order_confirm_combo/:id', component: OrderConfirmComboComponent },
  { path: 'order_tracking/:id/:part_id', component: TarackingComponent },
  { path: 'my_messeges', component: MessegesChatComponent },
  { path: 'wish_list', component: WishListComponent },
  
  { path: 'return_order/:id/:id1', component: ReturnOrderComponent },
  { path: 'cancel_order/:id/:id1', component: CancelOrderComponent },
  { path: 'view_order/:id/:id1', component: ViewOrderComponent },
  { path: 'rate_seller', component: RateSellerComponent },
  { path: 'product_review/:id/:id1', component: ProductReviewComponent },

  { path: 'event_ticket/:id', component: EventBootcampComponent },
  { path: 'event_cart', component: EventCartComponent },
  { path: 'event_confirm', component: EventConfirmComponent },
  { path: 'seller/:id', component: SellerProfileComponent },
  { path: 'event_home', component: EventHomeComponent },
  { path: 'chatInitiate/:id', component: ChatInitiateComponent },
  {path: 'auctionCheckout', component: AuctionCheckoutComponent },
  //{ path: '', redirectTo:'home', pathMatch: 'full'},
  { path: '', component: TestComponent },
  { path: 'paymentFailed', component: CancelPaymentComponent },
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
