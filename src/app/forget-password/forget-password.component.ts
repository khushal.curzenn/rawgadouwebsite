import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { UserServiceService } from '../user-service.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.css']
})
export class ForgetPasswordComponent implements OnInit {
  registerform !:FormGroup
  baseurl:any
  forgetPasswordApi:any; forgetPasswordApiRes:any;

  loading:Boolean= false;
  regdis:Boolean= false;
  constructor(private seller: UserServiceService, private http: HttpClient,) {
    this.baseurl = seller.baseapiurl2
    this.registerform = new FormGroup({
      email: new FormControl(''),
    })
    this.forgetPasswordApi = this.baseurl+"api/user/userForgotPassword"
   }

  ngOnInit(): void {
  }
  send(){
    this.loading = true
          this.regdis = true
    this.http.post(this.forgetPasswordApi ,this.registerform.value ).subscribe(res=>{
      this.forgetPasswordApiRes = res;
        this.loading = false;
          this.regdis = false;
      if(this.forgetPasswordApiRes.status == true)
      {

        setTimeout(()=>{
          window.location.reload();
        },2000)
      }
    })

  }

}
