import {
  Component,
  ElementRef,
  OnInit,
  QueryList,
  VERSION,
  ViewChildren
} from '@angular/core';
import { CurrencyPipe } from "@angular/common";
import { CartService } from '../cart.service';
import { HttpClient } from '@angular/common/http';
import { event } from 'jquery';
import { RouterModule, Routes } from '@angular/router';
import { UserServiceService } from '../user-service.service';
@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  @ViewChildren('subTotalWrap')
  subTotalItems!: QueryList<ElementRef>;
  @ViewChildren('subTotalWrap_existing')
  subTotalItems_existing!: QueryList<ElementRef>;
  public content = 'Available';
  products: any;
  items: any; baseurl: any; cartURL: any; user_id: any
  myoptions: any; gettedItems: any; removecarrURL: any; removecartoptions: any
  updateCartQuantity: any; updateCartQuantityRes: any;
  all_available: boolean = true; totalItem:number = 0;
  constructor(
    public cartService: CartService, private http: HttpClient,
    private currencyPipe: CurrencyPipe, private user: UserServiceService
  ) {
    this.baseurl = user.baseapiurl2
    this.cartURL = this.baseurl + "api/cart/getcartbyuserid"
    this.removecarrURL = this.baseurl + "api/cart/deleteproductfromcart"
    this.updateCartQuantity = this.baseurl + "api/cart/updateCartQuantity"
    this.user_id = localStorage.getItem('main_userid')
    this.products = [];
    this.gettedItems = []
    this.items = [];
    this.myoptions = {
      userid: this.user_id
    }
    this.removecartoptions = {
      userid: this.user_id
    }
    this.http.post(this.cartURL, this.myoptions).subscribe(res => {
      this.gettedItems = res;

      this.products = this.gettedItems.data;
      if (this.gettedItems.status == true) {
        this.all_available = this.gettedItems.all_available;
      }

      localStorage.setItem('cartcount', this.products.cart.length);
      //console.log( res,"adhgch");
      console.log("this.products ", this.products);
      if(this.products)
      {
        if(this.products.cart.length > 0)
        {
          for(let c=0; c<this.products.cart.length; c++)
          {
            console.log(this.products.cart[c].quantity)
            this.totalItem = this.totalItem + this.products.cart[c].quantity;
          }
        }
      }
    })
  }

  ngOnInit(): void {




  }


  changeSubtotal(item: { quantity: any; price: any; }, index: any) {

    const qty = item.quantity;
    const amt = item.price;
    const subTotal = amt * qty;
    let finalsubtotal = subTotal.toFixed(2);
    //  const subTotal_converted = this.currencyPipe.transform(subTotal, "USD");
    this.subTotalItems.toArray()[
      index
    ].nativeElement.innerHTML = finalsubtotal + ' ' + 'CFA';
    this.cartService.saveCart();
  }

  get total() {

    return this.products.cart.reduce(
      (sum: { price: number; }, x: { quantity: number; price: number; }) => ({
        quantity: 1,
        price: sum.price + x.quantity * x.price

      }),

      { quantity: 1, price: 0 }
    ).price.toFixed(2);

  }

  removeFromCart(val: any) {
    this.removecartoptions.productid = val
    console.log(this.removecartoptions)
    var con = confirm("Voulez-vous retirer ce produit du panier ?")
    if (con == true) {
      this.http.post(this.removecarrURL, this.removecartoptions).subscribe(res => {

        window.location.reload();

      })
    }
  }

  checkOut(event: any) {
    console.log(event)
  }

  updateQty(id: any, eve: any) {
    console.log(eve.target.value)
    console.log("id ", id);
    const params = {
      "user_id": this.user_id,
      "id": id,
      "quantity": eve.target.value

    }

    this.http.post(this.updateCartQuantity, params).subscribe(res => {
      this.updateCartQuantityRes = res
      if (this.updateCartQuantityRes.status) {
        window.location.reload()
      }
    })


  }

  numSequence(n: number): Array<number> {
    return Array(n);
  }

}
