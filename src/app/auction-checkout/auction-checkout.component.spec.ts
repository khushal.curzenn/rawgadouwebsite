import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AuctionCheckoutComponent } from './auction-checkout.component';

describe('AuctionCheckoutComponent', () => {
  let component: AuctionCheckoutComponent;
  let fixture: ComponentFixture<AuctionCheckoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AuctionCheckoutComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AuctionCheckoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
