import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RateSellerComponent } from './rate-seller.component';

describe('RateSellerComponent', () => {
  let component: RateSellerComponent;
  let fixture: ComponentFixture<RateSellerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RateSellerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RateSellerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
