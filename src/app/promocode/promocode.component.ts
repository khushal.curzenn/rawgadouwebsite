import { Component, OnInit } from '@angular/core';
import { UserServiceService } from '../user-service.service';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-promocode',
  templateUrl: './promocode.component.html',
  styleUrls: ['./promocode.component.css']
})
export class PromocodeComponent implements OnInit {
  baseurl:any;
  getAllPromoCode:any; getAllPromoCodeRes:any
  allPromocodes:any
  codeVisble:any
  myval:any
  constructor(private seller: UserServiceService, private http: HttpClient,
    private router: Router, private activateroute: ActivatedRoute) {
      this.baseurl = seller.baseapiurl2
      this.getAllPromoCode = this.baseurl+"api/seller/get_seller_coupon"
      this.codeVisble = true
      
     }

  ngOnInit(): void {

    this.http.get( this.getAllPromoCode).subscribe(res=>{
      this.getAllPromoCodeRes = res
      this.allPromocodes = this.getAllPromoCodeRes.data
      console.log(this.allPromocodes)
    })

  }

  
  showCode(i:any){
    this.codeVisble = !this.codeVisble;
    this.myval = i
  }
  copyText(val:any){
    alert("Texte copié !!!")
    let selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = val;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
  }

}
