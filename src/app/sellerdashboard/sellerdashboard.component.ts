import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sellerdashboard',
  templateUrl: './sellerdashboard.component.html',
  styleUrls: ['./sellerdashboard.component.css']
})
export class SellerdashboardComponent implements OnInit {
  openSidebar: boolean = true;

  menuSidebar = [
    {
      link_name: "Expédition",
      link: null,
      icon: "fa-solid fa-truck-fast",
      sub_menu: [
        {
          link_name: "Mon expédition",
          link: "shipment",
        }, {
          link_name: "Commandes à expédier",
          link: "ordertoship",
        }
      ]
    }, {
      link_name: "Commandez",
      link: null,
      icon: "fa-brands fa-first-order-alt",
      sub_menu: [
        {
          link_name: " Mes commandes",
          link: "myorders",
        }, {
          link_name: " Annulation",
          link: "/",
        }, {
          link_name: " Retour/Remboursement",
          link: "/",
        }
      ]
    },
    {
      link_name: " Produits",
      link: null,
      icon: "fa-brands fa-product-hunt",
      sub_menu: [
        {
          link_name: "  Mes produits",
          link: "myproducts",
        }, {
          link_name: "  Ajouter de nouveaux produits",
          link: "seller_addProduct",
        }, {
          link_name: "  Violations des produits",
          link: "/",
        }
      ]
    }
    ,
    {
      link_name: "  Finances",
      link: null,
      icon: "fa-solid fa-money-bill",
      sub_menu: [
        {
          link_name: " Mes revenus",
          link: "/",
        }, {
          link_name: " Mon équilibre ",
          link: "/",
        }, {
          link_name: "Compte bancaire",
          link: "/",
        }
      ]
    },
    {
      link_name: " Données",
      link: null,
      icon: "fa-solid fa-database",
      sub_menu: [
        {
          link_name: " Perspectives d'affaires",
          link: "/",
        }
      ]
    },
    {
      link_name: " Service à la clientèle",
      link: null,
      icon: "fa-solid fa-headset",
      sub_menu: [
        {
          link_name: "  Support de contact",
          link: "/",
        },
        {
          link_name: " FAQ Assistant ",
          link: "/",
        }
      ]
    },
    {
      link_name: " Shop",
      link: null,
      icon: "fa-solid fa-shop",
      sub_menu: [
        {
          link_name: "  Évaluation de la boutique",
          link: "/",
        },
        {
          link_name: " Profil de la boutique ",
          link: "/",
        },
        {
          link_name: " Mes rapports",
          link: "/",
        }
      ]
    },
    {
      link_name: " Centre de marketing",
      link: null,
      icon: "fa-solid fa-door-open",
      sub_menu: [
        {
          link_name: " Coupon de réduction",
          link: "/",
        }
      ]
    },
    {
      link_name: " Réglage de",
      link: null,
      icon: "fa-sharp fa-solid fa-gear",
      sub_menu: [
        {
          link_name: "  Mes adresses",
          link: "/",
        },
        {
          link_name: "  Paramètres de la boutique ",
          link: "/",
        },
        {
          link_name: "  Compte",
          link: "/",
        }
      ]
    }
  ]
  constructor() { }

  ngOnInit(): void {
  }

  showSubmenu(itemEl: HTMLElement) {

    itemEl.classList.toggle("showMenu");
  }


}
