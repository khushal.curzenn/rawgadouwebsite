import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderConfirmComboComponent } from './order-confirm-combo.component';

describe('OrderConfirmComboComponent', () => {
  let component: OrderConfirmComboComponent;
  let fixture: ComponentFixture<OrderConfirmComboComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrderConfirmComboComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OrderConfirmComboComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
