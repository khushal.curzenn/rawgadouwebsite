import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { SecurityComponent } from '../security/security.component';
import { UserServiceService } from '../user-service.service';
import { HttpClient } from '@angular/common/http';

export type DialogDataSubmitCallback<T> = (row: T) => void;

@Component({
  selector: 'app-add-name',
  templateUrl: './add-name.component.html',
  styleUrls: ['./add-name.component.css']
})
export class AddNameComponent implements OnInit {
  baseurl: any; useremail: any
  createuser: any; createuserRes: any
  myform!: FormGroup
  constructor(@Inject(MAT_DIALOG_DATA) public data: SecurityComponent,
    private seller: UserServiceService, private http: HttpClient,) {
    this.baseurl = seller.baseapiurl2
    this.useremail = localStorage.getItem('useremail')
    this.createuser = this.baseurl + "api/user/createuser"

    this.myform = new FormGroup({
      first_name: new FormControl(this.data.first_name, Validators.required),
      last_name: new FormControl(this.data.last_name, Validators.required)
    })
  }

  ngOnInit(): void {

  }
  send() {
    this.myform.value.email = this.useremail

    if (this.myform.valid) {
      this.http.post(this.createuser, this.myform.value).subscribe(res => {
        this.createuserRes = res

        if (this.createuserRes.status) {
          window.location.reload()
        }
      })

    }

    else {

      for (const control of Object.keys(this.myform.controls)) {
        this.myform.controls[control].markAsTouched();
      }
      return;

      // this.toastr.error("Les champs d'entrée sont requis", 'Error', {
      //   timeOut: 3000,
      // });
    }


  }

}
