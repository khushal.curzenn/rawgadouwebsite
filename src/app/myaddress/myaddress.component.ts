import { Component, OnInit } from '@angular/core';
import { UserServiceService } from '../user-service.service';
import { HttpClient } from '@angular/common/http';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { AddAddressComponent } from '../add-address/add-address.component';
import { EditAddressComponent } from '../edit-address/edit-address.component';
@Component({
  selector: 'app-myaddress',
  templateUrl: './myaddress.component.html',
  styleUrls: ['./myaddress.component.css']
})
export class MyaddressComponent implements OnInit {
  baseurl: any
  getAddressURL: any
  user_id: any
  myaddress: any
  myAddressForm:any
  deleteaddressURL: any;shippingURLres: any
  user_email:any;
  edit_address_id:any; last_login_time:any;
  constructor(private user: UserServiceService, private http: HttpClient,
    private dialog: MatDialog) {
    this.baseurl = user.baseapiurl2;

    this.user_id = localStorage.getItem('main_userid')
    let front_url = user.front_url;
    if(this.user_id === "" || this.user_id == null   || this.user_id == undefined )
    {
      window.location.href = front_url;
    }

    this.last_login_time = localStorage.getItem('last_login_time');
    console.log(" this.last_login_time " , this.last_login_time);
    if(this.last_login_time == "" || this.last_login_time == null || this.last_login_time == undefined ||  this.last_login_time == 'null' || this.last_login_time == 'undefined')
    {
      window.location.href = user.front_url+"login";
    }else{
      const date = new Date();
      //console.log("this.base_url"+this.base_url);
      var hours = Math.abs(date.getTime() - new Date(this.last_login_time).getTime()) / 3600000;
      console.log("hours "+hours);
      if(hours > 24)
      {
         localStorage.clear();
         window.location.href = user.front_url+"login";
      }
    }


    this.user_email = localStorage.getItem("useremail")
    this.user_id = localStorage.getItem("main_userid")
    this.getAddressURL = this.baseurl + "api/user/getuserprofile/"
    this.deleteaddressURL = this.baseurl + "api/user/deleteaddress"

    this.myAddressForm = {
      email: this.user_email
    }
  }

  ngOnInit(): void {
    this.getAddress()
  }

  getAddress() {
    this.http.get(this.getAddressURL + this.user_id).subscribe(res => {
      this.myaddress = res
      console.log(res)
    })
  }

  openDialog() {
    this.dialog.open(AddAddressComponent, {
      height: '650px',
      width: '600px',
    });
  }

  deleteAddress(id: any) {
    this.myAddressForm.addressid = id
    // console.log(this.myAddressForm)
    const conf = confirm("Voulez-vous supprimer l'adresse ?")
    if (conf == true) {
      this.http.post(this.deleteaddressURL, this.myAddressForm).subscribe(res => {
        this.shippingURLres = res
        if (this.shippingURLres.status == true) {
          
          window.location.reload()
        }
      })
    } else {
      
    }
  }
  editAddress(id:any)
  {
    // this.edit_address_id = id;
    // console.log("iddd ", id);
    this.dialog.open(EditAddressComponent, {
      height: '650px',
      width: '600px',
      data: {
        address_id: id,
      }
    }); 


  }
}
