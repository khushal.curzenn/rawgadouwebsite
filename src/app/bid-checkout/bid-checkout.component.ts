import { HttpClient } from '@angular/common/http';
import { Component, OnInit,HostListener } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { UserServiceService } from '../user-service.service';
import { Router,ActivatedRoute } from '@angular/router';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { AddAddressComponent } from '../add-address/add-address.component';
import { DOCUMENT } from '@angular/common';


@Component({
  selector: 'app-bid-checkout',
  templateUrl: './bid-checkout.component.html',
  styleUrls: ['./bid-checkout.component.css']
})
export class BidCheckoutComponent implements OnInit {
  CheckoutProducts: any; addressform!: FormGroup
  myAddressForm: any; user_email: any; baseurl: any; addressURL: any
  fulldata: any; getAddressURL: any; user_id: any
  myaddressArray: any; shippingURL: any; shippingURLres: any
  deleteaddressURL: any; beforeCheckeditem: any
  beforeCheckingURL: any; checkingoptions: any
  checkingitmes: any; checkingproducts: any; shippingCity: any;shippingAddress:any;
  checkoutURL: any; checkoutOptions: any; checkoutApires: any
  cartURL: any; cartOptions: any
  promoForm!: FormGroup

  globalChekArr: any
  showMymsg: any;
   showMymsg2:boolean = true;
  all_available:boolean=true;
  checkpromocode: any; checkpromocodeRes: any;classActive:string=""; classInActive:string="active";
  gettedItems:any; deliveryDefaultAddress:number=0;apiResponse:any;
  cash_on_delivery_var:number = 0; user_idddd:any;product_id:any;
  constructor(private ActiveRoute: ActivatedRoute,private user: UserServiceService, private http: HttpClient,
    private router: Router, private dialog: MatDialog,) {
      this.gettedItems = [];
    this.CheckoutProducts = []
    this.myaddressArray = []
    this.beforeCheckeditem = []
    this.checkingitmes = []
    this.checkingproducts = []
    this.baseurl = user.baseapiurl2
    this.cartURL = this.baseurl + "api/cart/getcartbyuserid";
    this.addressURL = this.baseurl + "api/user/createuser";
    this.getAddressURL = this.baseurl + "api/user/getuserprofile/";
    this.shippingURL = this.baseurl + "api/user/markaddressasshippingaddress";
    this.deleteaddressURL = this.baseurl + "api/user/deleteaddress";
    this.beforeCheckingURL = this.baseurl + "api/cart/beforecheckoutcart";
    this.checkoutURL = this.baseurl + "api/cart/checkoutcart";
    this.checkpromocode = this.baseurl + "api/cart/checkpromocode/";
    this.globalChekArr = []
    this.showMymsg = true

    this.user_idddd = this.ActiveRoute.snapshot.params['user_id'];
    this.product_id = this.ActiveRoute.snapshot.params['product_id'];

    this.user_email = localStorage.getItem("useremail")
    this.user_id = localStorage.getItem("main_userid")
    this.myAddressForm = {
      email: this.user_email
    }
    this.addressform = new FormGroup({
      country: new FormControl(''),
      name: new FormControl(''),
      pobox: new FormControl(''),
      telephone: new FormControl(''),
      address: new FormControl(''),
      city: new FormControl(''),

    })

    this.promoForm = new FormGroup({

      promocode: new FormControl(''),

    })
    
   
  }

  ngOnInit(): void {
    
  }

  
  
  applyPromo() {

    this.http.get(this.checkpromocode + this.promoForm.value.promocode).subscribe(res => {
      this.checkpromocodeRes = res
      if (this.checkpromocodeRes.status) {

        const checkingoptionsPromo = {
          id: this.user_id,
          shipping_city: this.checkingoptions.shipping_city,
          from: "cart",
          code: this.promoForm.value.promocode,
          shipping_id: this.globalChekArr
        }
        console.log(checkingoptionsPromo)

        this.http.post(this.beforeCheckingURL, checkingoptionsPromo).subscribe(res => {

          this.checkingitmes = res
          this.checkingproducts = this.checkingitmes.data

        })
      }
    })



  }

   
}
