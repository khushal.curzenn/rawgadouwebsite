import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BidCheckoutComponent } from './bid-checkout.component';

describe('BidCheckoutComponent', () => {
  let component: BidCheckoutComponent;
  let fixture: ComponentFixture<BidCheckoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BidCheckoutComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BidCheckoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
