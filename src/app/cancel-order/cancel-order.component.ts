import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserServiceService } from '../user-service.service';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-cancel-order',
  templateUrl: './cancel-order.component.html',
  styleUrls: ['./cancel-order.component.css']
})
export class CancelOrderComponent implements OnInit {
  part_id: any; table_id: any; baseurl: any
  get_order_detail_by_part_id: any; get_order_detail_by_part_idRes: any
  singleOrder: any
  mySelectedReason: any

  cancel_order_by_part_id:any; cancel_order_by_part_idRes:any

  constructor(private seller: UserServiceService, private http: HttpClient, private activateroute: ActivatedRoute,private router:Router) {
    this.baseurl = seller.baseapiurl2
    this.activateroute.paramMap.subscribe(params => {
      this.part_id = params.get('id');
      this.table_id = params.get('id1');

    });

    this.get_order_detail_by_part_id = this.baseurl + "api/order/get_order_detail_by_part_id"
    this.cancel_order_by_part_id = this.baseurl + "api/order/cancel_order_by_part_id"
    this.mySelectedReason = ''
  }

  ngOnInit(): void {
    const parms = {
      "id": this.part_id,
      "part_id": this.table_id
    }

    this.http.post(this.get_order_detail_by_part_id, parms).subscribe(res => {
      this.get_order_detail_by_part_idRes = res
      if (this.get_order_detail_by_part_idRes.status) {
        this.singleOrder = this.get_order_detail_by_part_idRes.data

       
      }
    })


  }
  selectReason(eve: any) {
    this.mySelectedReason = eve.target.value
  }
  cancel() {
    const parms = {
      "id":this.part_id ,
      "part_id": this.table_id,
      "returned_reason": this.mySelectedReason
    }
    console.log(this.cancel_order_by_part_id, parms)
    this.http.post(this.cancel_order_by_part_id, parms).subscribe(res=>{
      this.cancel_order_by_part_idRes = res
      console.log(this.cancel_order_by_part_idRes)
      if(this.cancel_order_by_part_idRes.status == true)
      {
        setTimeout(()=>{
          //window.location.reload();
          //product_order
          this.router.navigate(["/product_order"]);
        },1000);
      }
      
    })
  }

}
