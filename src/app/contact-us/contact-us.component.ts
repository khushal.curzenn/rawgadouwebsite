import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UserServiceService } from '../user-service.service';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {
  submitSellerContactForm!:FormGroup;baseurl:any;aboutAPI:any
  Apiresposnse:any
  userWebContactUs:any; userWebContactUsRes:any

  constructor(private user: UserServiceService, private http: HttpClient,) { 
    this.baseurl = user.baseapiurl2

    this.userWebContactUs =  this.baseurl+"api/userWebContactUs"

    this.submitSellerContactForm = new FormGroup({
      email: new FormControl('', [ Validators.required, Validators.email,Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]),
      name: new FormControl('',  Validators.required),
      phone: new FormControl('', [Validators.required]),
      message: new FormControl('',  Validators.required),

    })
  }

//   {
//     "name":"Kane",
//     "email":"kane789@mailinator.com",
//     "mobile":9878465456,
//     "message":"On sait depuis longtemps que travailler avec du texte lisible et contenant du sens est source de distractions, et empêche de se concentrer sur la mise en page elle-même. L'avantage du Lorem Ipsum sur un texte générique comme 'Du texte. Du texte. Du texte."
// }

  ngOnInit(): void {
  }
  
  sendEmail()
  {
    if (this.submitSellerContactForm.valid)
    {
      console.log(this.submitSellerContactForm.value)
      this.http.post(this.userWebContactUs, this.submitSellerContactForm.value).subscribe(res=>{
        this.userWebContactUsRes = res
        console.log(this.userWebContactUsRes)
        if(this.userWebContactUsRes.success){
          this.submitSellerContactForm.reset()
        }
      })
    }else{
      console.log("form in valid 84");
      for (const control of Object.keys(this.submitSellerContactForm.controls)) {
        this.submitSellerContactForm.controls[control].markAsTouched();
      }
      return;
    }

  }
}
