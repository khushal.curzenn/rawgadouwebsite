import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WonBidComponent } from './won-bid.component';

describe('WonBidComponent', () => {
  let component: WonBidComponent;
  let fixture: ComponentFixture<WonBidComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WonBidComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(WonBidComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
