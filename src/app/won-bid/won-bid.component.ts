import { Component, OnInit } from '@angular/core';
import { UserServiceService } from '../user-service.service';
import { HttpClient } from '@angular/common/http';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-won-bid',
  templateUrl: './won-bid.component.html',
  styleUrls: ['./won-bid.component.css']
})
export class WonBidComponent implements OnInit {
  user_id: any; baseurl: any; userorderURL: any; userOrders: any
  userorderRes: any; searchText: any
  adddate: any;

  getallprocessingordersbyuserid: any; getallprocessingordersbyuseridRes: any;
  processingRec: any

  getallcancelledordersbyuserid: any; getallcancelledordersbyuseridRes: any;
  cancelRec: any
  apiresponse:any;
  apiResponse:any;
  getallordersbyuseridPostMethod: any; getallordersbyuseridPostMethodRes: any
  selectedIndex: number=0;
  filterform!: FormGroup
  getMaximumReturnDay: any; maxDateres: any; maximumReturnDay: any
  totalPageNumber:number=0; numbers:any;
  totalPageNumberProcess:number=0; numbersProcess:any;selectedIndexProcess: number=0;
  totalPageNumberCancel:number=0; numbersCancel:any;selectedIndexCancel: number=0;
  last_login_time:any;
  constructor(private user: UserServiceService, private http: HttpClient,) {
    this.user_id = localStorage.getItem('main_userid')
    let front_url = user.front_url;
    if(this.user_id === "" || this.user_id == null   || this.user_id == undefined )
    {
      window.location.href = front_url;
    }


    this.last_login_time = localStorage.getItem('last_login_time');
    console.log(" this.last_login_time " , this.last_login_time);
    if(this.last_login_time == "" || this.last_login_time == null || this.last_login_time == undefined ||  this.last_login_time == 'null' || this.last_login_time == 'undefined')
    {
      window.location.href = user.front_url+"login";
    }else{
      const date = new Date();
      //console.log("this.base_url"+this.base_url);
      var hours = Math.abs(date.getTime() - new Date(this.last_login_time).getTime()) / 3600000;
      console.log("hours "+hours);
      if(hours > 24)
      {
         localStorage.clear();
         window.location.href = user.front_url+"login";
      }
    }


    this.baseurl = user.baseapiurl2
    this.userorderURL = this.baseurl + "api/order/getAllBidOrdersByUserId/"
    //this.getallprocessingordersbyuserid = this.baseurl + "api/order/getallprocessingordersbyuserid/" + this.user_id
    //this.getallcancelledordersbyuserid = this.baseurl + "api/order/getallcancelledordersbyuserid/" + this.user_id
    this.getMaximumReturnDay = this.baseurl + "api/product/getMaximumReturnDay"
    this.getallordersbyuseridPostMethod = this.baseurl + "api/order/getallordersbyuseridPostMethod"


    this.filterform = new FormGroup({
      start_date: new FormControl(''),
      end_date: new FormControl(''),

    })

    this.http.get(this.getMaximumReturnDay).subscribe(res => {
      this.maxDateres = res
      if (this.maxDateres.status) {
        this.maximumReturnDay = parseFloat(this.maxDateres.data[0].attribute_value);

        
      }

    })

    var d = new Date();
    d.setDate(d.getDate() + 5);
    

    //
    this.http.get(this.baseurl + "api/order/getAllBidOrdersByUserIdCount/" + this.user_id).subscribe(res => {
      this.apiResponse = res;
      console.log(" this.apiResponse ", this.apiResponse);
      this.totalPageNumber = this.apiResponse.totalPageNumber;
      this.numbers = Array(this.totalPageNumber).fill(0).map((m,n)=>n);
    })
     


  }

  ngOnInit(): void {
    this.getAllOrders(0)
   
  }


  getAllOrders(page_no:any) {
    this.selectedIndex = page_no;
    this.http.get(this.userorderURL + this.user_id+"/"+page_no).subscribe(res => {
      this.userorderRes = res
      this.userOrders = this.userorderRes.data
      console.log(this.userOrders, "all orders")


    })
  }
  
  filter() {

    this.filterform.value.userid = this.user_id
   

    let tt = new Date(this.filterform.value.start_date) ;
    let llll_g = new Date(tt.setDate(tt.getDate() + 1 ));
      let dateobj = new Date(llll_g);
      dateobj.setHours(0,0,0,0);
      // Contents of above date object is
      // converted into a string using toISOString() method.
      let start_date = dateobj.toISOString();


    let tt_r = new Date(this.filterform.value.end_date) ;
    let lll_g = new Date(tt_r.setDate(tt_r.getDate() + 1 ));
      let dateobj2 = new Date(lll_g);
      // Contents of above date object is
      // converted into a string using toISOString() method.
      let end_date = dateobj2.toISOString();

      const params = {
        userid: this.user_id,
        start_date: start_date,
        end_date: end_date,
      }
      console.log("start_date ", start_date);
      console.log("end_date ", end_date);



    this.http.post(this.getallordersbyuseridPostMethod, params).subscribe(res => {
      this.userorderRes = res
      this.userOrders = this.userorderRes.data
    })
  }
  getDateeee(dateee:any)
  {
    // console.log("dateee ", dateee);
     //console.log("this.maximumReturnDay  ", this.maximumReturnDay );
     
      // console.log("dateee ", dateee);
      /*var date = new Date(dateee);
      
      // Add five days to current date
      let llll_g = new Date(date.setDate(date.getDate() + this.maximumReturnDay));
       
      let dateobj = new Date(llll_g);
 
      // Contents of above date object is
      // converted into a string using toISOString() method.
      let B = dateobj.toISOString();
      let c = dateobj.getDate()+'/' + (dateobj.getMonth()+1) + '/'+dateobj.getFullYear();
      //console.log("c ", c);
   


        // this.adddate = new Date(dateee);
        // this.adddate.setDate(this.adddate.getDate() + this.maximumReturnDay);
        
        
        // let llll_g = new Date(this.adddate.getTime() + this.maximumReturnDay*24*60*60*1000);
 
        return c; */
        //return this.adddate;
        //console.log("ddddddddddddd " , this.adddate);
    //{{baseurl}}api/event/createOrderInvoice/{{order.order_id}}

  }
  downloadOrderInvoice(order_idd:any)
  {
    this.http.get(this.baseurl+"api/event/createOrderInvoice/"+order_idd).subscribe(res => {
      this.apiresponse = res
       if(this.apiresponse.status == true)
       {
          //window.location.href = this.apiresponse.url;
          window.open(this.apiresponse.url, 'download');  
       }
    })
  }

}
