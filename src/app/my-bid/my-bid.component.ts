import { Component, HostListener, Input, OnInit } from '@angular/core';
import { ViewportScroller } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { UserServiceService } from '../user-service.service';
import { Slick } from 'ngx-slickjs';
//import { map, takeWhile, timer } from 'rxjs';
import { interval, Observable } from "rxjs";
import { map, shareReplay } from "rxjs/operators";

@Component({
  selector: 'app-my-bid',
  templateUrl: './my-bid.component.html',
  styleUrls: ['./my-bid.component.css']
})
export class MyBidComponent implements OnInit {
  // @Input() seconds = 300;

  // timeRemaining$ = timer(0, 1000).pipe(
  //   map(n => (this.seconds - n) * 1000),
  //   takeWhile(n => n >= 0),
  // );
  baseurl: any; bannerURL: any; bannerData: any
  topCategoriesURL: any; topCategoriesData: any
  feauturedCategoriesURL: any; feauturedCategoriesData: any
  firstBannerImg: any
  addFavProduct: any; addFavProductRes: any
  homepro: any
  user_id: any
  deleteFavProduct: any; deleteFavProductRes: any
  getMiddlebannerimages: any; smallBanners: any
  middlebanner: any
  mydate:any

  days:any; hours:any; mins:any; seconds:any

  showTime: any;
  public timerInterval: any;
  timeLeft$:any;
  //public timeLeft$: Observable<timeComponents>;
  hostUrl:string="";
  topProducts:any; tProducts:any; topProductsres:any;  last_login_time:any;
  constructor(private viewportScroller: ViewportScroller, private http: HttpClient, private user: UserServiceService,) {
    this.baseurl = user.baseapiurl2
    this.user_id = localStorage.getItem('main_userid')
    this.hostUrl = user.front_url;
    if(this.user_id == "" || this.user_id == null || this.user_id == undefined)
    {
      window.location.href=this.hostUrl+"login"
    }

    this.last_login_time = localStorage.getItem('last_login_time');
    console.log(" this.last_login_time " , this.last_login_time);
    if(this.last_login_time == "" || this.last_login_time == null || this.last_login_time == undefined ||  this.last_login_time == 'null' || this.last_login_time == 'undefined')
    {
      window.location.href = user.front_url+"login";
    }else{
      const date = new Date();
      //console.log("this.base_url"+this.base_url);
      var hours = Math.abs(date.getTime() - new Date(this.last_login_time).getTime()) / 3600000;
      console.log("hours "+hours);
      if(hours > 24)
      {
         localStorage.clear();
         window.location.href = user.front_url+"login";
      }
    }


    this.addFavProduct = this.baseurl + "api/product/addFavProduct"
    this.deleteFavProduct = this.baseurl + "api/product/deleteFavProduct/" 

    this.topProducts = this.baseurl + "api/product/allYourBidProduct"
    this.homepro = []
    
    // this.timeLeft$ = interval(1000).pipe(
    //   map(x => this.calcDateDiff()),
    //   shareReplay(1)
      
    // );
     
  }
  //calcDateDiff(endDay: Date = new Date(2023, 9, 13)  )
  calcDateDiff(endDay: any )
  {
    //console.log("today date ", new Date() );
    //  endDay = new Date( endDay.setHours(12, 44, 53))
      endDay = new Date(endDay)
    console.log("endDay ", endDay);
    const dDay = endDay.valueOf();
    

    const milliSecondsInASecond = 1000;
    const hoursInADay = 24;
    const minutesInAnHour = 60;
    const secondsInAMinute = 60;
  
    const timeDifference = dDay - Date.now();
  
    const daysToDday = Math.floor(
      timeDifference /
        (milliSecondsInASecond * minutesInAnHour * secondsInAMinute * hoursInADay)
    );
  
    const hoursToDday = Math.floor(
      (timeDifference /
        (milliSecondsInASecond * minutesInAnHour * secondsInAMinute)) %
        hoursInADay
    );
  
    const minutesToDday = Math.floor(
      (timeDifference / (milliSecondsInASecond * minutesInAnHour)) %
        secondsInAMinute
    );
  
    const secondsToDday =
      Math.floor(timeDifference / milliSecondsInASecond) % secondsInAMinute;
  
    return { secondsToDday, minutesToDday, hoursToDday, daysToDday };
  }
  getArray(count: number) {
    return new Array(count)
  }

  ngOnInit(): void {
    
    this.convertTime();
   this.getTopProducts()
    
  }

   
  addFavItem(productid: any) {

    const parms = {
      "product_id": productid,
      "user_id": this.user_id
    }

    this.http.post(this.addFavProduct, parms).subscribe(res => {
      this.addFavProductRes = res

      if (this.addFavProductRes.status) {
        window.location.reload()
      }
    })

  }
  removeFavItem(productid: any) {

    this.http.get(this.deleteFavProduct + productid).subscribe(res => {
      this.deleteFavProductRes = res
      console.log(res)

      if (this.deleteFavProductRes.status) {
        window.location.reload()
      }
    })
  }

   

  convertTime(){
    const countDownDate = new Date("oct 30, 2023 15:37:25").getTime();

    setInterval(() => {
      
    }, 1000);
  }

 

  
  getTopProducts(){
    this.http.post(this.topProducts , { "user_id":this.user_id }).subscribe(res=>{
      this.topProductsres = res
      if(this.topProductsres.status){
        this.tProducts =  this.topProductsres.data
      }
      console.log(res, "rajesh")
    })
  }





}
