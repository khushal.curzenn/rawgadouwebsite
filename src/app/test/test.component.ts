import { Component, HostListener, OnInit } from '@angular/core';
import { ViewportScroller } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { UserServiceService } from '../user-service.service';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import { trigger, transition, animate, style } from '@angular/animations'
import { Slick } from 'ngx-slickjs';
@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css'],
  animations: [
    trigger('fade', [
      transition('void => *', [style({ opacity: 0 }), animate('300ms', style({ opacity: 1 }))]),
      transition('* => void', [style({ opacity: 1 }), animate('300ms', style({ opacity: 0 }))]),
    ])
  ]
})
export class TestComponent implements OnInit {
  baseurl: any; bannerURL: any; bannerData: any
  topCategoriesURL: any; topCategoriesData: any
  feauturedCategoriesURL: any; feauturedCategoriesData: any
  firstBannerImg: any; secondBannerImg: any
  getMiddlebannerimages: any; smallBanners: any
  middlebanner: any
  arrayLength = 10;
  products: any
  allcatProducts: any

  getallproducts: any; getallproductsRes: any
  getallproductsRes2: any

  config: Slick.Config = {
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 0,
    dots: false,
    autoplay: true,
    autoplaySpeed: 2000,
    accessibility: false,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 1008,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        },
      },

    ],
    nextArrow: "<button class='btn ' style='position: absolute !important;right: 0 !important;top: 50% !important;z-index: 2 !important;background: #eaeded; color: rgba(0, 0, 0, 0.4);height: 75px; border: none !important; width: 50px;margin: 0;border-radius: 0;outline: none !important;box-shadow: none !important;'><i class='fa fa-angle-right' styele='font-size:5rem !important'></i></button>",
    prevArrow: "<button  class='btn '  style='position: absolute !important;left: 0 !important;top: 50% !important;z-index: 2 !important;background: #eaeded; color: rgba(0, 0, 0, 0.4);height: 75px; border: none !important; width: 50px;margin: 0;border-radius: 0;outline: none !important;box-shadow: none !important;'><i class='fa fa-angle-left' styele='font-size:5rem !important'></i></button>",
  }

  getArray(count: number) {
    return new Array(count)
  }


  constructor(private viewportScroller: ViewportScroller, private http: HttpClient, private user: UserServiceService,
    config: NgbCarouselConfig) {
    this.baseurl = user.baseapiurl2
    this.bannerURL = this.baseurl + "api/admin/getallbannerimage"
    this.topCategoriesURL = this.baseurl + "api/admin/gettopcatagories"
    this.feauturedCategoriesURL = this.baseurl + "api/admin/getFeatruedProductcatagory"
    this.getMiddlebannerimages = this.baseurl + "api/admin/getMiddlebannerimages"
    this.getallproducts = this.baseurl + "api/product/getallproducts"
    config.interval = 2000;
    config.keyboard = true;
    config.pauseOnHover = true;
    this.products = []
    this.allcatProducts = []
  }

  ngOnInit(): void {
    this.getBannerImages()
    this.getTopCategories()
    this.getFeaturedCategories()
    this.getMiddleBanner()
    this.getAllProducts()

  }

  getBannerImages() {
    this.http.get(this.bannerURL).subscribe(res => {
      this.bannerData = res
      const firstBanner = this.bannerData.data.filter((x: any) => {
        return x.location == 'user_home_first_block'
      })
      const secondBanner = this.bannerData.data.filter((x: any) => {
        return x.location == 'user_home_second_block'
      })
      if (firstBanner) {
        this.firstBannerImg = firstBanner

      }
      if (secondBanner) {
        this.secondBannerImg = secondBanner
      }


    })
  }
  getTopCategories() {
    this.http.get(this.topCategoriesURL).subscribe(res => {
      this.topCategoriesData = res


    })
  }

  getFeaturedCategories() {
    this.http.get(this.feauturedCategoriesURL).subscribe(res => {
      this.feauturedCategoriesData = res
      // console.log(this.feauturedCategoriesData, "tjis is data")

    })
  }

  getMiddleBanner() {
    this.http.get(this.getMiddlebannerimages).subscribe(res => {
      this.smallBanners = res
      this.middlebanner = this.smallBanners.data

      console.log(this.middlebanner, "dfhdfsudjheujh")

    })
  }

  getAllProducts() {

    const parms = {
      "catagory": "iphone",
      "singleid": "",
      "sort": "",
      "pricerangeLow": "",
      "pricerangeHigh": "",
      "rating": "",
      "condition": ""
    }
    this.http.post(this.getallproducts, parms).subscribe(res => {
      this.getallproductsRes2 = res
      if (this.getallproductsRes2.status) {
        this.products = this.getallproductsRes2.data.products
      }

    })

    const parms2 = {
      "catagory": "intel",
      "singleid": "",
      "sort": "",
      "pricerangeLow": "",
      "pricerangeHigh": "",
      "rating": "",
      "condition": ""
    }
    this.http.post(this.getallproducts, parms2).subscribe(res => {
      this.getallproductsRes = res
      if (this.getallproductsRes.status) {
        this.allcatProducts = this.getallproductsRes.data.products
        console.log( this.allcatProducts, "adhadgh")
      }

    })



  }
}
