import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UserServiceService } from '../user-service.service';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-wish-list',
  templateUrl: './wish-list.component.html',
  styleUrls: ['./wish-list.component.css']
})
export class WishListComponent implements OnInit {
  baseurl:any; userId:any
  getFavProduct:any; getFavProductRes:any
  favItemList:any
  deleteFavProduct:any; deleteFavProductRes:any
  constructor(private seller: UserServiceService, private http: HttpClient,
    private router: Router, private activateroute: ActivatedRoute) { 
      this.baseurl = seller.baseapiurl2
      this.userId = localStorage.getItem("main_userid");
      this.getFavProduct = this.baseurl+"api/product/getFavProduct/"+this.userId
      this.deleteFavProduct = this.baseurl+"api/product/deleteFavProduct/"
    }

  ngOnInit(): void {

    this.http.get(this.getFavProduct).subscribe(res=>{
      this.getFavProductRes = res

      if(this.getFavProductRes.status){
        this.favItemList = this.getFavProductRes.record

        console.log(this.favItemList , "sdjkfh")
      }
    })
  }
  removeFav(id:any){
    console.log(this.deleteFavProduct+id)

     this.http.get(this.deleteFavProduct+id).subscribe(res=>{
      this.deleteFavProductRes = res
      
      if( this.deleteFavProductRes.status){
        window.location.reload()
      }
     })
  }

}
