import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EventLikeComponent } from './event-like.component';

describe('EventLikeComponent', () => {
  let component: EventLikeComponent;
  let fixture: ComponentFixture<EventLikeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EventLikeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EventLikeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
