import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';


@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {
  public loginform !: FormGroup
  visible: boolean = true;
  changetype: boolean = false;
  constructor() { }

  ngOnInit(): void {
    this.loginform = new FormGroup({
     
      email :new FormControl(''),
      
    })
  }
  viewpass() {
    this.visible = !this.visible;
    this.changetype = !this.changetype;
  }
  login(){
    console.log(this.loginform.value)
  }
}
