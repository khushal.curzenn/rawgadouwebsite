import { Component, OnInit,HostListener } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UserServiceService } from '../user-service.service';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-seller-profile',
  templateUrl: './seller-profile.component.html',
  styleUrls: ['./seller-profile.component.css']
})
export class SellerProfileComponent implements OnInit {
  baseurl:any;mainid:any
  get_seller_product:any
  apires:any; sellerInfo:any
  fullname:any;created_at:any;image:any
  sellerProducts:any;
  api_response_var:number=0; ordersarray:any; base_url:string="";
  totalPageNumber:number=1;pageStart:number=0;totalAdcount:number=0;pageNo:number=1;
  constructor(private seller: UserServiceService, private http: HttpClient,
    private router: Router, private activateroute: ActivatedRoute) { 
      this.baseurl = seller.baseapiurl2;
      this.base_url = seller.baseapiurl
      this.mainid = this.activateroute.snapshot.params['id']
      this.get_seller_product = this.baseurl+"api/seller/get_seller_product/"+this.mainid

    }

  ngOnInit(): void {
    this.http.get(this.get_seller_product).subscribe(res=>{
      this.apires = res

      if(this.apires.status){
        this.sellerInfo = this.apires.data[0]
        this.sellerProducts = this.apires.data[1].product
        this.fullname = this.sellerInfo.fullname
        this.created_at = this.sellerInfo.created_at
        this.image = this.baseurl+"static/public/"+this.sellerInfo.shopphoto
        console.log( this.sellerInfo)
      }
    })
  }

  @HostListener('window:scroll', [])
  onScroll(): void 
  {
    console.log("in bottom");
    
  }


}
