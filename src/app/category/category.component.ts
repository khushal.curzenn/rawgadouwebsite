import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { UserServiceService } from '../user-service.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {
  items:any;
  p: any;
  baseurl:any
  navURL:any
  myoptions:any
  categoryURL: any
  parentCat: any
  constructor(private http: HttpClient,private user: UserServiceService) {
    this.baseurl = user.baseapiurl2
    this.navURL =this.baseurl+"api/product/listProducts"
    this.categoryURL = this.baseurl + "api/product/getOnlyParentCategory"
    this.myoptions = {
        category_id:"",
        pagno:0,
        sort:1,
        pricerangeLow:"",
        pricerangeHigh:"",
        rating:"",
        condition:""
    
    }
   }

  ngOnInit(): void {
    this.getProducts();
    this.getParentCategory()
  }
  getProducts(){
    this.http.post(this.navURL,this.myoptions).subscribe(res=>{
      this.items =  res
      console.log(this.items)
    })
    
  }
  getParentCategory() {
    this.http.get(this.categoryURL).subscribe(res => {
      this.parentCat = res

      console.log(this.parentCat)
    })
  }
}
