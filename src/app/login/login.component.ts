import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UserServiceService } from '../user-service.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginform!: FormGroup
  baseurl: any
  myoption: any
  getstatus:any;loginurl:any
  visible: boolean = true;
  changetype: boolean = false;
  submitted = false;
  constructor(private user: UserServiceService, private http: HttpClient,
    private router: Router) {
    this.baseurl = user.baseapiurl2
    this.loginurl =  this.baseurl+"api/user/login"
    this.loginform = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email,Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]),
      password: new FormControl('',  Validators.required)
    })
   
  }

  ngOnInit(): void {
    localStorage.clear()
  }
  // login() {
  //   this.loginform.value.fcm_token = ''
  //   console.log(this.loginform.value)
  //   this.http.post(this.loginurl, this.loginform.value).subscribe(res => {
  //      this.getstatus = res
  //      console.log(this.getstatus)
  //      if(this.getstatus.status == true){
  //       localStorage.setItem('main_userid', this.getstatus.data._id);
  //       localStorage.setItem('useremail', this.loginform.value.email);
  //       alert(this.getstatus.message)
  //       this.router.navigate(['home'])
  //      }else{
  //       alert(this.getstatus.errmessage)
  //      }
      
  //   })

  // }

  viewpass() {
    this.visible = !this.visible;
    this.changetype = !this.changetype;
  }
  
  get email() {
    return this.loginform.get('email')!;
  }
 

 
  login() {
    this.submitted = true
    this.loginform.value.fcm_token = ''
   
    if(this.loginform.valid ){
      this.http.post(this.loginurl, this.loginform.value).subscribe(res => {
        this.getstatus = res
        
        if(this.getstatus.status == true)
        {

           
          // console.log("this.getstatus.data ",this.getstatus.data.last_login_time);
          // return
          localStorage.setItem('main_userid', this.getstatus.data._id);
          localStorage.setItem('token', this.getstatus.data.token);
          localStorage.setItem('useremail', this.loginform.value.email);
          localStorage.setItem('last_login_time', this.getstatus.data.last_login_time);
          localStorage.setItem('loginsession', '24');
          //  this.toastr.success(this.getstatus.message, 'Success', {
          //   timeOut: 3000,
          // });
          this.router.navigate(['/'])
        }else{
        }
       
     })
    }else{

      for (const control of Object.keys(this.loginform.controls)) {
        this.loginform.controls[control].markAsTouched();
      }
      return;
      
      // this.toastr.error("Les champs d'entrée sont requis", 'Error', {
      //   timeOut: 3000,
      // });
    }
   

  }

}
