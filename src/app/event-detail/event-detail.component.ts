import { Component, OnInit } from '@angular/core';
import { Route, ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { UserServiceService } from '../user-service.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-event-detail',
  templateUrl: './event-detail.component.html',
  styleUrls: ['./event-detail.component.css']
})
export class EventDetailComponent implements OnInit {
  products: any;
  single_product: any;
  eventdetailURL: any
  baseurl: any
  eventOptions: any
  eventid: any
  user_id: any
  event_details: any
  addEvent: any; eventres: any; apiResponse:any;

  savefavriout: any; savefavrioutRes: any
  getRandombannerbyid: any; getRandombannerbyidRes: any; bannerImg: any
  deletefavoriteApi:any; deletefavoriteApires:any
  constructor(private ActiveRoute: ActivatedRoute, private http: HttpClient,
    private user: UserServiceService, private router: Router) {
    this.baseurl = user.baseapiurl2
    this.products = []
    this.single_product = []
    this.eventid = this.ActiveRoute.snapshot.params['id']
    this.user_id = localStorage.getItem("main_userid")
    this.eventdetailURL = this.baseurl + "api/event/geteventbyid"
    this.addEvent = this.baseurl + "api/event/addEventtocart"
    this.eventOptions = {
      user_id: this.user_id,
      event_id: this.eventid
    }

    this.savefavriout = this.baseurl + "api/event/savefavriout";
    this.deletefavoriteApi = this.baseurl + "api/event/deletefavorite";

  }

  ngOnInit(): void {
    // console.log(this.ActiveRoute.snapshot.params['id'])
    this.findItems()
  }

  findItems() {

    this.http.post(this.eventdetailURL, this.eventOptions).subscribe(res => {
      this.event_details = res
      console.log(this.event_details)
    })
  }
  onTicket(eve: any) {


    const ticketOptions = {
      userid: this.user_id,
      eventid: this.eventid,
      "price": eve.data[0].tickets[0].price,
      "quantity": 1,
      "ticketype": eve.data[0].type
    }

    this.http.post(this.addEvent, ticketOptions).subscribe(res => {
      
      this.eventres = res;
      this.apiResponse = res;
      if (this.eventres.status == true) {
        this.router.navigate(['/event_cart'])
        //window.location.reload()
      }
    })
  }

  addFav(id: any) {

    const params = {
      "user_id": this.user_id,
      "event_id": id
    }
    this.http.post(this.savefavriout, params).subscribe(res => {
      this.savefavrioutRes = res
      if (this.savefavrioutRes.status) {
        window.location.reload()
      }
    })

  }
  removeFav(id:any){
    console.log(id)
    const params={
      "user_id": this.user_id ,
      "event_id":id
    }
    this.http.post(this.deletefavoriteApi, params).subscribe(res=>{
      this.deletefavoriteApires = res
      if(this.deletefavoriteApires.status){
        window.location.reload()
      }
    })
  }

}
